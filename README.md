# Task Fluent

## Preparation

### Update & Dependencies

```sh
apt-get update
apt-get upgrade -y
apt-get install -y build-essential patch zlib1g-dev liblzma-dev libssl-dev curl wget
```

### Node

```sh
curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
npm install --global yarn @angular/cli
```

### Rails

```sh
apt-get install -y ruby ruby-dev
gem install rails
```

### MariaDB

```sh
apt install -y mariadb-server libmariadbclient-dev
mysql -u root
```

```sql
UPDATE mysql.user SET plugin = 'mysql_native_password', Password = PASSWORD('') WHERE User = 'root';
FLUSH PRIVILEGES;
EXIT;
```

### NGINX

```sh
sudo apt-get install -y nginx
```

`/etc/nginx/sites-enabled/default`

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name taskfluent.<DA>.system.shawinigan.info;

	location / {
		proxy_pass http://127.0.0.1:3000;
	}
}
```

```sh
service nginx reload
```

### systemd

`/etc/systemd/system/taskfluent.service`

```systemd
[Unit]
Description=Task Fluent
After=network.target mysqld.service

[Service]
Type=simple
User=root
WorkingDirectory=<PATH_TO_PROJECT>/Rails/
ExecStart=<PATH_TO_PROJECT>/Rails/bin/rails s
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

## Start / Restart

```sh
systemctl stop taskfluent
cd Rails && bundler install && bin/rake db:drop db:create db:migrate db:seed && cd ..
cd Angular && npm install && ng build --prod && cd ..
systemctl start taskfluent
```
