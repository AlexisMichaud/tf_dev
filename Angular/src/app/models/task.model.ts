import { custom, JSONObject, optional, required } from "ts-json-object";

export class Task extends JSONObject {
  @optional
  id!:number

  @optional
  project_id!:number

  @required
  name!: string;

  @required
  description!: string;

  @required
  state!: number;

  @required
  estimated_time!: number;

  @optional
  actual_time!: number;

  @required
  @custom( (task:Task,key:string,value:string) => {
    return new Date(value)
  })
  due_date!: Date;

  @optional
  time_ratio!: number;

}
