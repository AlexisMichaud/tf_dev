import { custom, JSONObject, optional, required } from "ts-json-object";

export class Team extends JSONObject {
  @optional
  id!: number;

  @required
  name!: string;

  @required
  description!: string;

  @optional
  message!: string;

  @optional
  @custom( (team:Team,key:string,value:string) => {
    return new Date(value)
  })
  message_expiration!: Date;

  @optional
  owner_id!: number;

}
