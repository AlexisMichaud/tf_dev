import { JSONObject, optional, required } from "ts-json-object";

export class JobOffer extends JSONObject {
  @optional
  id!: number;

  @optional
  name!: string;

  @optional
  description!: string;

  @optional
  availability!: number;

  @optional
  nb_employee_looking_for!: number;

}
