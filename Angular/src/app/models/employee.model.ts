import { custom, JSONObject, optional, required } from "ts-json-object";

export class Employee extends JSONObject {
  @required
  @custom((employee: Employee, key: string, value: string) => {
    return value.toLowerCase();
  })

  email!: string;

  @required
  firstname!: string;

  @required
  lastname!: string;

  @optional
  fullname: string;

  @optional
  id: number;

  @optional
  address: string;

  @optional
  phone: string;

  @optional
  availability: number;

  getFullName = () => this.firstname + ' ' + this.lastname
}
