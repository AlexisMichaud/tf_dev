import { custom, JSONObject, optional, required } from "ts-json-object";

export class Membership extends JSONObject {
  @optional
  id!: number;


  @optional
  user_id!: number;


  @optional
  team_id!: number;
}
