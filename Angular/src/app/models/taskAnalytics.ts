import { JSONObject, optional, required } from "ts-json-object";

export class TaskAnalytics extends JSONObject {
  @optional
  id!:number

  @optional
  activePercent!:string

  @optional
  percentOverTime!: string;

  @optional
  avgTime!: string;

  @optional
  worktimeCount!: number;

  @optional
  mostTime!: number;

  @optional
  mostTimeName!: string;

  @optional
  percentProblem!:string

}
