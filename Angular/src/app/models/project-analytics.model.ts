import { JSONObject, required, optional } from "ts-json-object";

export class ProjectAnalytics extends JSONObject {
  @required
  active_percentage?: number;

  @required
  archived!: boolean;

  @required
  avg_meeting_time?: number;

  @required
  avg_meeting_count?: number;

  @optional
  id!: number;

  @required
  meetings_count?: number;

  @required
  most_meetings_count?: number;

  @required
  most_meetings_project?: string;

  @required
  name!: string;

  @required
  tot_meetings?: number;
}
