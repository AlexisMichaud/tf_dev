import { JSONObject, optional, required } from 'ts-json-object';
import { Borrow } from './borrow.model';

export class Resource extends JSONObject {
  @optional
  id?: number;

  @required
  brand!: string;

  @required
  model!: string;

  @required
  description!: string;

  @optional
  state_name!: string;

  @optional
  state_id!: number;

  @optional
  borrows?: Borrow[];
}
