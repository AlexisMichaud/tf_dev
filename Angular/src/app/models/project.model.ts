import { custom, JSONObject, optional, required } from "ts-json-object";
import { Meeting } from "./meeting.model";

export class Project extends JSONObject {
  @optional
  id!: number;

  @required
  name!: string;

  @required
  description!: string;

  @required
  due_date!: Date;

  @optional
  archived!: boolean;

  @optional
  meetings?: Meeting[];

  @optional
  meetings_count?: number;

  @optional
  tot_meetings?: number;

  @optional
  active_percentage?: number;

  @optional
  avg_meeting_time?: number;
}
