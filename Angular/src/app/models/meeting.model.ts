import { custom, JSONObject, optional, required } from "ts-json-object";

export class Meeting extends JSONObject {
  @optional
  id!: number;

  @required
  name!: string;

  @required
  summary!: string;

  @required
  location!: string;

  @required
  start_dt!: Date;

  @required
  end_dt!: Date;

  @optional
  archived!: boolean;

  @optional
  project_id!: number;
}
