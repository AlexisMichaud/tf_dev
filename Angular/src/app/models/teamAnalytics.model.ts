import { JSONObject, optional, required } from "ts-json-object";

export class TeamAnalytics extends JSONObject { 
  @optional
  team_id!: number;

  @optional
  name!: string;

  @optional
  age!: number;

  @optional
  numberOfMember!: number;

  @optional
  firstname!: string;

  @optional
  lastname!: string;

  @optional
  numberOfPost!: number;

  @optional
  nbOfFullTime!: number;

  @optional
  nbOfHalfTime!: number;

  @optional
  nbOfContractual!: number;

}