import { JSONObject, optional, required } from "ts-json-object";

export class Worktime extends JSONObject {
  @optional
  id!:number

  @optional
  task_id!:number

  @optional
  user_id!:number

  @optional
  user!:string

  @required
  name!: string;

  @required
  description!: string;

  @optional
  problem!:string

  @required
  start_dt!: Date;

  @optional
  end_dt!: Date;

  @optional
  duration!:number

  @optional
  archived!: boolean;
}
