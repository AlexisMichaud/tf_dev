import { JSONObject, optional, required } from 'ts-json-object';

export class Borrow extends JSONObject {
  @required
  id!: number;

  @required
  resource_id!: number;

  @required
  user_id!: number;

  @required
  start_date!: string;

  @optional
  end_date?: string;

  @optional
  state_name?: string;

  @optional
  state_id?: number;

  @required
  user_fullname!: string;
}
