import { JSONObject, optional, required } from 'ts-json-object';

export class ResourceAnalytics extends JSONObject {
  @optional
  id?: number;

  @required
  brand!: string;

  @required
  model!: string;

  @required
  description!: string;

  @optional
  state_name?: string;

  @optional
  state_id?: number;

  @required
  borrow_count!: number;

  @required
  days_borrowed!: number;

  @optional
  top_borrower?: string;
}
