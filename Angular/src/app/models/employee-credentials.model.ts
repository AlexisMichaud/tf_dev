export interface EmployeeCredentials {
  email: string;

  password: string;
  lastname: string;
  firstname: string;
}
