interface SuccessResponse<T> {
  success: true;
  data: T;
}
interface ErrorResponse<E> {
  success: false;
  data: E;
}
interface Exception {
  success: false;
  exception: any;
}

type Json = string | number | boolean | null | Json[] | { [key: string]: Json };

type ModelErrors<T> = T extends Record<string, any>
  ? { [F in keyof T]?: T[F] extends Json ? string[] : T[F] }
  : unknown;
export type Errors<T> =
  | (T extends Array<infer E> ? ModelErrors<E> : ModelErrors<T>)
  | string;

export type Response<T, E = Errors<T>> =
  | SuccessResponse<T>
  | ErrorResponse<E>
  | Exception;
export default Response;
