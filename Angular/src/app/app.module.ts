import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from './material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProjectsComponent } from './components/alexis/projects/projects.component';
import { ProjectDetailComponent } from './components/alexis/project-detail/project-detail.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TeamComponent } from './components/jf/team/team.component';
import { TasksComponent } from './components/kevin/tasks/tasks.component';
import { ResourcesComponent } from './components/raph/resources/resources.component';
import { TaskDetailComponent } from './components/kevin/task-detail/task-detail.component';
import { MeetingDetailComponent } from './components/alexis/meeting-detail/meeting-detail.component';
import { ResourceDetailComponent } from './components/raph/resource-detail/resource-detail.component';
import { NewTeamComponent } from './components/jf/new-team/new-team-component';
import { TeamBoardMembersComponent } from './components/jf/team-board-members/team-board-members.component';
import { TeamEditBoardAllUsersComponent } from './components/jf/team-edit-board-all-users/team-edit-board-all-users.component';
import { WorktimesComponent } from './components/kevin/worktimes/worktimes.component';
import { WorktimesDetailComponent } from './components/kevin/worktimes-detail/worktimes-detail.component';
import { ResourceStateDialogComponent } from './components/raph/resource-state-dialog/resource-state-dialog.component';
import { JobOffersComponent } from './components/jf/job-offers/job-offers.component';
import { JobOfferDetailComponent } from './components/jf/job-offer-detail/job-offer-detail.component';
import { ProjectsAnalyticsComponent } from "./components/alexis/projects-analytics/projects-analytics.component";
import { TeamAnalyticsComponent } from './components/jf/team-analytics/team-analytics.component';
import { TaskAnalyticComponent } from './components/kevin/task-analytic/task-analytic.component';
import { ResourceAnalyticsComponent } from './components/raph/resource-analytics/resource-analytics.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    DialogComponent,
    ProfileComponent,

    ProjectsComponent,
    ProjectDetailComponent,
    MeetingDetailComponent,
    ProjectsAnalyticsComponent,

    TasksComponent,
    TaskDetailComponent,
    WorktimesComponent,
    WorktimesDetailComponent,

    ResourcesComponent,
    ResourceDetailComponent,
    ResourceStateDialogComponent,

    TeamComponent,
    NewTeamComponent,
    TeamBoardMembersComponent,
    TeamEditBoardAllUsersComponent,
    JobOffersComponent,
    JobOfferDetailComponent,
    TeamAnalyticsComponent,
    TaskAnalyticComponent,
    ResourceAnalyticsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
