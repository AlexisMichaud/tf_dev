import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { TeamService } from "../services/team.service";

@Injectable({
  providedIn: 'root'
})
export class TeamGuard implements CanActivateChild {
  constructor(private router: Router, private teamService: TeamService) { }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.teamService.getTeam().pipe(map(success => success == false ? this.router.parseUrl('/team') : true))
  }
}
