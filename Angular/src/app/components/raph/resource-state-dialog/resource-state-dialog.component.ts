import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-resource-state-dialog',
  templateUrl: './resource-state-dialog.component.html',
  styleUrls: ['./resource-state-dialog.component.css'],
})
export class ResourceStateDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}
