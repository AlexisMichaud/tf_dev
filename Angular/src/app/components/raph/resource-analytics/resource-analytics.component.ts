import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ResourcesService } from 'src/app/services/resources.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ResourceAnalytics } from 'src/app/models/resource-analytics.model';
import Response from 'src/app/models/response.model';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-resource-analytics',
  templateUrl: './resource-analytics.component.html',
  styleUrls: ['./resource-analytics.component.css'],
})
export class ResourceAnalyticsComponent implements OnInit {
  resources: ResourceAnalytics[] = [];
  dataSource: MatTableDataSource<ResourceAnalytics>;
  displayedColumns: string[] = [
    'model',
    'brand',
    'description',
    'state_id',
    'borrow_count',
    'days_borrowed',
    'top_borrower',
  ];
  sForm: FormGroup;

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public authService: AuthService,
    private resourcesService: ResourcesService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.sForm = new FormGroup({
      search: new FormControl(route.snapshot.queryParams.search ?? ''),
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((p) => this.updateFilters(p, this));
  }

  private fetch() {
    const search = this.sForm.get('search').value.toString().toLowerCase();
    this.resourcesService
      .analytics(search ? search : undefined)
      .subscribe((res) => {
        if (res.success === true) {
          this.resources = res.data;
          this.refreshDataTable();
        } else {
          this.handleError(res);
        }
      });
  }

  private updateFilters(params: Params, self = this) {
    const search = self.sForm.get('search');

    const searchValue = params.search ? params.search : '';
    search.setValue(searchValue);

    self.fetch();
  }

  updateQuery() {
    const search = this.sForm.get('search').value.toString().toLowerCase();

    this.router.navigate(['/resources/analytics'], {
      queryParams: { search: search ? search : undefined },
    });
  }

  private refreshDataTable() {
    this.dataSource = new MatTableDataSource(this.resources);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  private handleError(res: Response<ResourceAnalytics[]>) {
    if ('data' in res) {
      if (typeof res.data === 'string') {
        this.snackBar.open(res.data, 'Dismiss');
      } else {
        for (const field in res.data) {
          const errors = res.data;
          for (const error in errors) {
            this.snackBar.open(`${field}: ${error}`, 'Dismiss');
          }
        }
      }
    } else {
      this.snackBar.open('An error occurred', 'Dismiss');
    }
  }
}
