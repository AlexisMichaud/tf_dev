import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Params, Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ResourcesService } from 'src/app/services/resources.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Resource } from 'src/app/models/resource.model';
import Response from 'src/app/models/response.model';

interface Status {
  value: string;
  label: string;
}

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css'],
})
export class ResourcesComponent implements OnInit {
  statusVals: Status[] = [
    { value: 'available', label: 'Available' },
    { value: 'mine', label: 'Mine' },
    { value: 'all', label: 'All' },
  ];
  ssfForm: FormGroup;
  resources: Resource[] = [];
  dataSource: MatTableDataSource<Resource>;
  displayedColumns: string[] = ['model', 'brand', 'description', 'state_id'];

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public authService: AuthService,
    private router: Router,
    private resourcesService: ResourcesService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.ssfForm = new FormGroup({
      status: new FormControl(route.snapshot.queryParams.status ?? 'available'),
      search: new FormControl(route.snapshot.queryParams.search ?? ''),
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((p) => this.updateFilters(p, this));
  }

  private fetchResources() {
    const search = this.ssfForm.get('search').value.toString().toLowerCase();
    const status = this.ssfForm.get('status').value.toString().toLowerCase();
    this.resourcesService
      .index(search ? search : undefined, status)
      .subscribe((response) => {
        if (response.success === true) {
          this.resources = response.data;
          this.refreshDataTable();
        } else {
          this.handleError(response);
        }
      });
  }

  private updateFilters(params: Params, self = this) {
    const search = self.ssfForm.get('search');
    const status = self.ssfForm.get('status');

    const searchValue = params.search ? params.search : '';
    search.setValue(searchValue);

    const statusValue = self.statusVals.some((v) => v.value === params.status)
      ? params.status
      : 'available';
    status.setValue(statusValue);

    self.fetchResources();
  }

  updateQuery() {
    const search = this.ssfForm.get('search').value.toString().toLowerCase();
    const status = this.ssfForm.get('status').value.toString().toLowerCase();

    this.router.navigate(['/resources'], {
      queryParams: { search: search ? search : undefined, status },
    });
  }

  private refreshDataTable() {
    this.dataSource = new MatTableDataSource(this.resources);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  edit(row: { id: number }) {
    this.router.navigate(['/resources', row.id]);
  }

  private handleError(res: Response<Resource> | Response<Resource[]>) {
    if ('data' in res) {
      if (typeof res.data === 'string') {
        this.snackBar.open(res.data, 'Dismiss');
      } else {
        for (const field in res.data) {
          const errors = res.data;
          for (const error in errors) {
            this.snackBar.open(`${field}: ${error}`, 'Dismiss');
          }
        }
      }
    } else {
      this.snackBar.open('An error occurred', 'Dismiss');
    }
  }
}
