import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Resource } from 'src/app/models/resource.model';
import { ResourcesService } from 'src/app/services/resources.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Borrow } from 'src/app/models/borrow.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from 'src/app/services/auth.service';
import Response from 'src/app/models/response.model';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../dialog/dialog.component';
import { ResourceStateDialogComponent } from '../resource-state-dialog/resource-state-dialog.component';

@Component({
  selector: 'app-resource-detail',
  templateUrl: './resource-detail.component.html',
  styleUrls: ['./resource-detail.component.css'],
})
export class ResourceDetailComponent implements OnInit {
  form = new FormGroup({
    brand: new FormControl({ value: null, disabled: true }, [
      Validators.required,
      Validators.maxLength(45),
    ]),
    model: new FormControl({ value: null, disabled: true }, [
      Validators.required,
      Validators.maxLength(45),
    ]),
    description: new FormControl({ value: null, disabled: true }, [
      Validators.required,
      Validators.maxLength(255),
    ]),
  });
  editing = false;
  id?: number;
  resource: Resource;
  previous?: Resource;

  dataSource: MatTableDataSource<Borrow>;
  displayedColumns: string[] = [
    'start_date',
    'end_date',
    'user_fullname',
    'state_name',
  ];

  @ViewChild(MatSort) sort: MatSort = new MatSort();
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public resourcesService: ResourcesService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  get isMine() {
    return (
      this.authService.currentEmployee &&
      this.resource?.borrows?.some(
        (borrow) =>
          borrow.user_id === this.authService.currentEmployee.id &&
          !borrow.end_date
      )
    );
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id') === 'new') {
      this.toggle();
    } else {
      const id = Number(this.route.snapshot.paramMap.get('id'));
      if (Number.isNaN(id)) {
        this.router.navigate(['/resources']);
        return;
      }
      this.id = id;
      this.fetch();
    }
  }

  private fetch() {
    this.resourcesService.show(this.id).subscribe((res) => {
      if (res.success === true) {
        this.resource = res.data;

        const { brand, model, description } = this.resource;
        this.form.setValue({ brand, model, description });

        this.dataSource = new MatTableDataSource(this.resource.borrows);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      } else {
        this.handleError(res);
      }
    });
  }

  toggle() {
    this.editing = !this.editing;
    if (this.editing) {
      this.previous = this.form.value;

      this.form.get('model')?.enable();
      this.form.get('brand')?.enable();
      this.form.get('description')?.enable();
    } else {
      this.form.get('model')?.disable();
      this.form.get('brand')?.disable();
      this.form.get('description')?.disable();
    }
  }

  submit() {
    const resource = new Resource(this.form.value);
    if (this.id === undefined) {
      this.resourcesService.create(resource).subscribe((res) => {
        if (res.success === true) {
          this.resource = res.data;
          this.id = res.data.id;

          const { brand, model, description } = this.resource;
          this.form.setValue({ brand, model, description });

          this.toggle();
          this.router.navigate(['/resources', res.data.id]);
        } else {
          this.handleError(res);
        }
      });
    } else {
      this.resourcesService.update(this.id, resource).subscribe((res) => {
        if (res.success === true) {
          this.toggle();
        } else {
          this.handleError(res);
        }
      });
    }
  }

  cancel() {
    this.toggle();
    this.form.setValue(this.previous);
  }

  remove() {
    if (!this.id) {
      return;
    }

    this.dialog
      .open(DialogComponent, {
        autoFocus: false,
        data: {
          title: 'Confirm deletion?',
          message:
            'Are you sure you wish to delete this resource and all its associated history?',
          true: 'Yes',
          false: 'Cancel',
        },
      })
      .afterClosed()
      .subscribe((result) => {
        if (result === 'true') {
          this.resourcesService.delete(this.id).subscribe((res) => {
            if (res.success === true) {
              this.router.navigate(['/resources']);
            } else {
              this.handleError(res);
            }
          });
        }
      });
  }

  borrow() {
    if (!this.id || this.isMine) {
      return;
    }

    this.resourcesService.borrow(this.id).subscribe((res) => {
      if (res.success === true) {
        this.router.navigate(['/resources'], {
          queryParams: { status: 'mine' },
        });
      } else {
        this.handleError(res);
      }
    });
  }

  unborrow() {
    if (!this.id || !this.isMine) {
      return;
    }

    this.dialog
      .open(ResourceStateDialogComponent, {
        autoFocus: false,
        data: {
          title: 'Resource state',
          message:
            'What state is this resource in at the moment you are returning it?',
          states: [
            { label: 'New', value: 0 },
            { label: 'Used', value: 1 },
            { label: 'Damaged', value: 2 },
          ],
        },
      })
      .afterClosed()
      .subscribe((result) => {
        this.resourcesService.unborrow(this.id, result).subscribe((res) => {
          if (res.success === true) {
            this.router.navigate(['/resources'], {
              queryParams: { status: 'available' },
            });
          } else {
            this.handleError(res);
          }
        });
      });
  }

  private handleError(res: Response<Resource> | Response<Resource[]>) {
    if ('data' in res) {
      if (typeof res.data === 'string') {
        this.snackBar.open(res.data, 'Dismiss');
      } else {
        for (const field in res.data) {
          const errors = res.data;
          for (const error in errors) {
            this.snackBar.open(`${field}: ${error}`, 'Dismiss');
          }
        }
      }
    } else {
      this.snackBar.open('An error occurred', 'Dismiss');
    }
  }
}
