import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AuthedGuard } from 'src/app/guards/authed.guard';
import { HomeComponent } from '../home/home.component';
import { JobOffersComponent } from '../jf/job-offers/job-offers.component';
import { LoginComponent } from '../login/login.component';
import { ProfileComponent } from '../profile/profile.component';
import { ProjectDetailComponent } from '../alexis/project-detail/project-detail.component';
import { ProjectsComponent } from '../alexis/projects/projects.component';
import { ResourcesComponent } from '../raph/resources/resources.component';
import { SignupComponent } from '../signup/signup.component';
import { TasksComponent } from '../kevin/tasks/tasks.component';
import { TeamComponent } from '../jf/team/team.component';
import { TaskDetailComponent } from '../kevin/task-detail/task-detail.component';
import { MeetingDetailComponent } from '../alexis/meeting-detail/meeting-detail.component';
import { ResourceDetailComponent } from '../raph/resource-detail/resource-detail.component';
import { NewTeamComponent } from '../jf/new-team/new-team-component';
import { WorktimesDetailComponent } from '../kevin/worktimes-detail/worktimes-detail.component';
import { TeamGuard } from 'src/app/guards/team.guard';
import { JobOfferDetailComponent } from '../jf/job-offer-detail/job-offer-detail.component';
import { TeamAnalyticsComponent } from '../jf/team-analytics/team-analytics.component';
import { TaskAnalyticComponent } from '../kevin/task-analytic/task-analytic.component';
import { ProjectsAnalyticsComponent } from '../alexis/projects-analytics/projects-analytics.component';
import { ResourceAnalyticsComponent } from '../raph/resource-analytics/resource-analytics.component';

export const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthedGuard],
    children: [
      { path: '', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
    ],
  },
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'profile', component: ProfileComponent },

      { path: 'team', component: TeamComponent },
      { path: 'team/analytics', component: TeamAnalyticsComponent },
      { path: 'team/new', component: NewTeamComponent },
      { path: 'team/:id', component: TeamComponent },

      { path: 'jobOffers', component: JobOffersComponent },
      { path: 'jobOffer/:id', component: JobOfferDetailComponent },
      { path: 'resources', component: ResourcesComponent },
      { path: 'resources/analytics', component: ResourceAnalyticsComponent },
      { path: 'resources/:id', component: ResourceDetailComponent },

      {
        path: '',
        canActivateChild: [TeamGuard],
        children: [
          { path: 'tasks', component: TasksComponent },
          { path: 'task/:id', component: TaskDetailComponent },
          { path: 'task/:tid/worktime/:id', component: WorktimesDetailComponent },
          { path: 'task/:tid/worktime/new', component: WorktimesDetailComponent },
          { path: 'tasks/analytics', component: TaskAnalyticComponent },
          { path: 'projects', component: ProjectsComponent },
          { path: 'project/:id', component: ProjectDetailComponent },
          { path: 'project/new', component: ProjectDetailComponent },
          {
            path: 'project/:id/meeting/:id',
            component: MeetingDetailComponent,
          },
          {
            path: 'project/:id/meeting/new',
            component: MeetingDetailComponent,
          },
          { path: 'project/:id/tasks', component: TasksComponent },
          { path: 'project/:pid/task/:id', component: TaskDetailComponent },
          { path: 'project/:pid/task/new', component: TaskDetailComponent },

          { path: 'projects/analytics', component: ProjectsAnalyticsComponent },
        ],
      },
    ],
  },
];
