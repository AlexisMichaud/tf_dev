import { Component, OnInit } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {
  title = 'tf-ng';
  mediaSub!: Subscription;
  deviceXs!: boolean;
  navOpen = false;

  constructor(public authService: AuthService, private router: Router, public mediaObserver: MediaObserver) {
    this.authService.isLoggedIn().subscribe();
  }



  ngOnInit(): void {
    this.mediaSub = this.mediaObserver.media$.subscribe(
      (result: MediaChange) => {
        this.deviceXs = result.mqAlias === 'xs' ? true : false;
        console.log(result.mqAlias)
      }
    );
  }

  logout() {
    this.authService.logOut().subscribe((success) => {
      if (success) {
        this.router.navigate(['/']);
      }
    });

  }
}
