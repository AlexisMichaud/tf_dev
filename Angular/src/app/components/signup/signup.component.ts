import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  uniqueEmailError: string | null = null;
  signupForm: FormGroup;
  constructor(private authService: AuthService, private router: Router) {
    this.signupForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      firstname: new FormControl(null, [Validators.required]),
      lastname: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      password_confirmation: new FormControl()
    }, this.passwordMatch);

  }

  private passwordMatch(form: AbstractControl): ValidationErrors | null {
    if (form.value?.password != form.value?.password_confirmation) {
      return { passwordConfirmationMustMatch: true };
    } else {
      return null
    }
  }

  ngOnInit(): void {
  }

  signUp() {
    this.authService.signUp(this.signupForm.value).subscribe(success => {
      console.log('Sign up component', success);

      if (success) {
        this.router.navigate(['/']);
      } else {
        this.setUniqueEmailError(true);
      }
    });
  }


  onEmailInput() {
    if (this.uniqueEmailError) {
      this.setUniqueEmailError(false);

      this.signupForm.get('email')?.updateValueAndValidity();
    }
  }

  private setUniqueEmailError(isUsed: boolean) {
    this.uniqueEmailError = isUsed ? 'This email is already used!' : null;
    this.signupForm.get('email')?.setErrors(isUsed ? { emailNotUnique: true } : null);
  }
}


