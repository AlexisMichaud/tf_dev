import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Employee } from '../../models/employee.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  editForm: FormGroup;
  editToggle: boolean = false;
  avatar: string;

  constructor(public authService: AuthService) {
    this.editForm = new FormGroup({
      phone: new FormControl(
        { value: this.authService.currentEmployee.phone, disabled: true },
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
        ]
      ),
      email: new FormControl(
        { value: this.authService.currentEmployee.email, disabled: true },
        [Validators.required, Validators.email]
      ),
      address: new FormControl(
        { value: this.authService.currentEmployee.address, disabled: true },
        [Validators.required]
      ),
    });

    this.avatar = `https://ui-avatars.com/api/?name=${this.authService.currentEmployee.firstname}+${this.authService.currentEmployee.lastname}&size=300`;
    this.fetchAvatar();

    console.log(this.authService.currentEmployee);
  }

  avatarChange(e: Event) {
    const input: HTMLInputElement = e.target as HTMLInputElement;
    const file = input.files[0];
    if (!file) {
      return;
    }

    const reader = new FileReader();
    reader.onload = () => {
      this.authService
        .updateAvatar(reader.result as string)
        .subscribe((res) => {
          if (res.success === true) {
            this.avatar = new URL(res.data.avatar).pathname;
          }
        });
    };
    reader.readAsDataURL(file);
  }

  private fetchAvatar() {
    this.authService.showAvatar().subscribe((res) => {
      if (res.success === true) {
        this.avatar = new URL(res.data.avatar).pathname;
      }
    });
  }

  ngOnInit(): void {}

  submit() {
    this.authService
      .update(
        new Employee({
          phone: this.editForm.get('phone').value,
          email: this.editForm.get('email').value,
          address: this.editForm.get('address').value,
          firstname: this.authService.currentEmployee.firstname,
          lastname: this.authService.currentEmployee.lastname,
        })
      )
      .subscribe((success) => {
        if (success) {
          console.log('OK ', success);
          this.disableForm();
          this.editToggle = !this.editToggle;
        } else {
          console.log('Error ', success);
        }
      });
  }

  edit() {
    if (!this.editToggle) {
      this.enableForm();
    } else {
      const {
        firstname,
        lastname,
        fullname,
        availability,
        getFullName,
        ...u
      } = this.authService.currentEmployee;
      this.editForm.setValue(u);
      this.disableForm();
    }
    this.editToggle = !this.editToggle;
  }

  private enableForm() {
    this.editForm.get('phone')?.enable();
    this.editForm.get('email')?.enable();
    this.editForm.get('address')?.enable();
  }
  private disableForm() {
    this.editForm.get('phone')?.disable();
    this.editForm.get('email')?.disable();
    this.editForm.get('address')?.disable();
  }
}
