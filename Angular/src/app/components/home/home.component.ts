import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Home, HomeService } from 'src/app/services/home.service';
import Response from 'src/app/models/response.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  home?: Home;
  message?: string;

  constructor(
    public authService: AuthService,
    private homeService: HomeService,
    private snackBar: MatSnackBar
  ) {
    this.fetch();
  }

  private fetch() {
    this.homeService.index().subscribe((res) => {
      if (res.success === true) {
        this.home = res.data;

        const messageExp = new Date(this.home.team.message_expiration);
        const now = new Date();
        if (messageExp > now) {
          this.message = this.home.team.message;
        }
      } else {
        this.handleError(res);
      }
    });
  }

  private handleError(res: Response<Home>) {
    if ('data' in res) {
      if (typeof res.data === 'string') {
        this.snackBar.open(res.data);
      } else {
        for (const field in res.data) {
          const errors = res.data;
          for (const error in errors) {
            this.snackBar.open(`${field}: ${error}`, 'Dismiss');
          }
        }
      }
    } else {
      this.snackBar.open('An error occurred', 'Dismiss');
    }
  }
}
