import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Worktime } from 'src/app/models/worktime.model';
import { LocationService } from 'src/app/services/location.service';
import { WorktimeService } from 'src/app/services/worktime.service';
import { DialogComponent } from '../../dialog/dialog.component';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-worktimes-detail',
  templateUrl: './worktimes-detail.component.html',
  styleUrls: ['./worktimes-detail.component.css']
})
export class WorktimesDetailComponent implements OnInit {
  editForm: FormGroup;
  editToggle: boolean = false;
  maxDate;
  endMinDate;
  endMinTime;
  currentWorktime: Worktime;
  id !: string | null;

  constructor(public route: ActivatedRoute, private router: Router, private dialog: MatDialog, public worktimeService: WorktimeService, private locationService: LocationService,private authService:AuthService) {
    this.editForm = new FormGroup({
      name: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(45)]),
      description: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(255)]),
      user: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(255)]),
      problem: new FormControl({ value: null, disabled: true }, [Validators.maxLength(255)]),
      start_dt: new FormControl({ value: null, disabled: true },[Validators.required]),
      end_dt: new FormControl({ value: null, disabled: true }),
    }); }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id') != "new") {
      this.id = this.route.snapshot.paramMap.get('id');
      this.worktimeService.getWorktime(this.id).subscribe(success => {
        if (success) {
          this.currentWorktime = success as Worktime
          this.maxDate = new Date()
          this.maxDate.setMinutes(this.maxDate.getMinutes()+1)
          this.endMinDate = new Date(success.start_dt)
          const {archived,user_id, ...worktimeInfo} = this.currentWorktime
          this.editForm.setValue(worktimeInfo)
        } else {
          console.log("Invalid Task");
        }
      })
    } else if (this.route.snapshot.paramMap.get('id') == "new") {
      this.edit()
      this.maxDate = new Date()
      this.maxDate.setMinutes(this.maxDate.getMinutes()+1)
      this.endMinDate = new Date()
    }

  }

  openDialog(action: string) {
    let dialogRef;
    if (action == 'delete') {
      dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm deletion?',
          message: 'Are you sure you wish to delete this worktime?',
          true: 'Yes',
          false: 'Cancel'
        }
      });
    }  else if (action=='archive') {
      dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm archiving?',
          message: 'Are you sure you wish to archive this worktime?',
          true: 'Yes',
          false: 'Cancel'
        }
      });
    }

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'true' && action == 'delete') {
        this.worktimeService.deleteWorktime(this.id).subscribe(success => {
          if (success) {
            this.locationService.goBack()
          } else {
            console.log("Could not delete worktime!");
          }
        });
      }else if (result == 'true' && action == 'archive') {
        this.worktimeService.archiveWorktime(this.id,this.currentWorktime).subscribe(success => {
          if (success) {
            this.locationService.goBack()
          } else {
            console.log("Could not archive worktime!");
          }
        });
      }
    })
  }

  edit() {
    if (this.editToggle) {
      this.editForm.get('name')?.disable();
      this.editForm.get('description')?.disable();
      this.editForm.get('problem')?.disable();
      this.editForm.get('start_dt')?.disable();
      this.editForm.get('end_dt')?.disable();
    } else {
      this.editForm.get('name')?.enable();
      this.editForm.get('description')?.enable();
      this.editForm.get('problem')?.enable();
      this.editForm.get('start_dt')?.enable();
      this.editForm.get('end_dt')?.enable();
    }
    this.editToggle = !this.editToggle;
  }
  submit() {
    if (this.id == null) {
      let newWorktime = this.editForm.value
      newWorktime.user_id=this.authService.currentEmployee.id
      newWorktime.task_id = this.route.snapshot.paramMap.get('tid')
      this.worktimeService.createWorktime(newWorktime).subscribe(success => {
        if (success) {
          this.currentWorktime=success
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.navigate(['/task/' + this.route.snapshot.paramMap.get('tid') + '/worktime/'+this.currentWorktime.id]);
        } else {
          console.log(success.error);
        }
      });
    } else {
      let newWorktime = this.editForm.value
      newWorktime.user_id=this.authService.currentEmployee.id
      this.worktimeService.updateWorktime(this.id,newWorktime).subscribe(success => {
        if (success) {
          this.currentWorktime=success
          this.edit()
        } else {
          console.log(success.error);
        }
      });
    }
  }
  cancel(){
    this.edit()
    if(this.currentWorktime!=null){
      const {archived,user_id,task_id, ...worktimeInfo}=this.currentWorktime
      this.editForm.setValue(worktimeInfo)
    }
    else
      this.leave()
  }
  leave() {
    if (this.route.snapshot.paramMap.get('tid'))
      this.router.navigate(['/task/' + this.route.snapshot.paramMap.get('tid') ]);
  }
}
