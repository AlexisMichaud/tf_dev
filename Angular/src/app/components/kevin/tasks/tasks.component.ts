import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { Task } from 'src/app/models/task.model';
import { TaskService } from 'src/app/services/task.service';
interface Ssf {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit {
  tasks: Task[] = [];
  //Assigning displayed columns
  displayedColumns: string[] = ['name', 'description', 'due_date', 'state','estimated_time','actual_time'];
  dataSource;
  id;
  selectedValue: string;
  selectedFilter: string | '';
  selectedSearch: string | null;
  filterVals: Ssf[] = [
    { value: 'all', viewValue: 'All' },
    { value: '0', viewValue: 'In Progress' },
    { value: '1', viewValue: 'Finished' },
    { value: '2', viewValue: 'Archived' },
  ];
  ssfForm: FormGroup;
  currentDate:Date=new Date()
  nextWeek:Date=new Date()


  taskState(state: number) {
    if (state == 0)
      return "In Progress"
    if (state == 1)
      return "Finished"
    if (state == 2)
      return "Archived"
    return null
  }

  constructor(public route: ActivatedRoute, private router: Router, private taskService: TaskService) {
    this.ssfForm = new FormGroup({
      filterOpts: new FormControl('all'),
      search: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.nextWeek.setDate(this.currentDate.getDate()+7)
    if (this.route.snapshot.paramMap.get('id')) {
      this.id = this.route.snapshot.paramMap.get('id');
      this.getProjectTasks(this.id)
    } else {
      this.getAllTasks()
    }
    this.route.queryParams.subscribe(params => {
      this.ssfForm.get('filterOpts').setValue(params.state);
      this.ssfForm.get('search').setValue(params.search);

      this.selectedValue = this.ssfForm.get('filterOpts').value;

      if (params.state == null) this.ssfForm.get('filterOpts').setValue('all');
      if (params.search == null) this.ssfForm.get('search').setValue('');
    });

  }



  edit(row: any) {
    if (this.route.snapshot.paramMap.get('id'))
      this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('id') + '/task/' + row.id]);
    else
      this.router.navigate(['/task/' + row.id]);
  }

  applyFilter(column: string, filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.tasks);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  stateFilter() {
    var isFilterOk = false;
    this.selectedFilter = this.ssfForm.get('filterOpts').value?.toString();
    this.filterVals.forEach(v => {
      if (v.value == this.selectedFilter) isFilterOk = true;
    });

    if (isFilterOk) {
      this.selectedValue = this.selectedFilter;
      this.selectedSearch = this.ssfForm.get('search').value?.toString().toLowerCase();
      if (this.route.snapshot.paramMap.get('id')) {
        if (this.selectedSearch) {
          this.taskService.ssfProjectTasks("state=" + this.selectedFilter + "&search=" + this.selectedSearch, this.id).subscribe(response => {
            if(response){
            this.tasks = response as Task[];
            }else{
              this.tasks=Task[0]
            }
            this.initDataTable();
          });
        } else {
          this.taskService.ssfProjectTasks("state=" + this.selectedFilter, this.id).subscribe(response => {
            if(response){
            this.tasks = response as Task[];
            }else{
              this.tasks=Task[0]
            }
            this.initDataTable();
          });
        }
      }
      else {
        if (this.selectedSearch) {
          this.taskService.ssfTask("state=" + this.selectedFilter + "&search=" + this.selectedSearch).subscribe(response => {
            if(response){
            this.tasks = response as Task[];
            }else{
              this.tasks=Task[0]
            }
            this.initDataTable();
          });
        } else {
          this.taskService.ssfTask("state=" + this.selectedFilter).subscribe(response => {
            if(response){
            this.tasks = response as Task[];
            }else{
              this.tasks=Task[0]
            }
            this.initDataTable();
          });
        }
      }
    }
  }

  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  newTask() {
    this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('id') + '/task/' + 'new']);
  }

  getProjectTasks(project_id: string) {
    this.taskService.getProjectTasks(project_id).subscribe(success => {
      if (success) {
        this.tasks = success as Task[]
        this.initDataTable();
      } else {
        console.log("Couldnt load Tasks")
      }
    });
  }
  getAllTasks() {
    this.taskService.getAllTasks().subscribe(success => {
      if (success) {
        this.tasks = success as Task[]
        this.initDataTable();
      } else {
        ("Couldnt load tasks")
      }
    });
  }


}
