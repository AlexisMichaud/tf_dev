import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { DialogComponent } from "../../dialog/dialog.component";
import { Task } from 'src/app/models/task.model';
import { LocationService } from 'src/app/services/location.service';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})

export class TaskDetailComponent implements OnInit {
  editForm: FormGroup;
  editToggle: boolean = false;
  minDate= new Date();
  currentTask: Task;
  id !: string | null;

  constructor(public route: ActivatedRoute, private router: Router, private dialog: MatDialog, public taskService: TaskService, private locationService: LocationService) {
    this.editForm = new FormGroup({
      name: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(45)]),
      description: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(255)]),
      estimated_time: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.min(15), Validators.pattern("^[0-9]*$"),]),
      actual_time: new FormControl({ value: null, disabled: true }),
      due_date: new FormControl({ value: null, disabled: true }, [Validators.required]),
      state: new FormControl({ value: null, disabled: true }, [Validators.required])
    });
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id') != "new") {
      this.id = this.route.snapshot.paramMap.get('id');
      this.taskService.getTask(this.id).subscribe(success => {
        if (success) {
          this.currentTask = success
          this.editForm.setValue(this.currentTask)
        } else {
          console.log("Invalid Task");
        }
      }
      )
    } else if (this.route.snapshot.paramMap.get('id') == "new") {
      this.edit()
    }
  }

  openDialog(action: string) {
    let dialogRef;
    if (action == 'delete') {
      dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm deletion?',
          message: 'Are you sure you wish to delete this task and all its associated elements?',
          true: 'Yes',
          false: 'Cancel'
        }
      });
    } else if (action=='archive') {
      dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm archiving/unarchiving?',
          message: 'Are you sure you wish to archive/unarchive this task and all its associated elements?',
          true: 'Yes',
          false: 'Cancel'
        }
      });
    }

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'true' && action == 'delete') {
        this.taskService.deleteTask(this.id).subscribe(success => {
          if (success) {
            this.locationService.goBack()
          } else {
            console.log("Could not delete task!");
          }
        });
      }else if (result == 'true' && action == 'archive') {
        this.taskService.archiveTask(this.id,this.currentTask).subscribe(success => {
          if (success) {
            this.locationService.goBack()
          } else {
            console.log("Could not archive!");
          }
        });
      }
    })
  }


  edit() {
    if (this.editToggle) {
      this.editForm.get('name')?.disable();
      this.editForm.get('description')?.disable();
      this.editForm.get('due_date')?.disable();
      this.editForm.get('estimated_time')?.disable();
      this.editForm.get('state')?.disable();
    } else {
      this.editForm.get('name')?.enable();
      this.editForm.get('description')?.enable();
      this.editForm.get('due_date')?.enable();
      this.editForm.get('estimated_time')?.enable();
      this.editForm.get('state')?.enable();
    }
    this.editToggle = !this.editToggle;
  }

  submit() {
    if (this.id == null) {
      let newTask = this.editForm.value
      newTask.project_id = this.route.snapshot.paramMap.get('pid')
      this.taskService.createTask(newTask).subscribe(success => {
        if (success) {
          this.currentTask=success
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('pid') + '/task/'+this.currentTask.id]);
        } else {
          console.log("Could not create task!");
        }
      });
    } else {
      let newTask = this.editForm.value
      this.taskService.updateTask(this.id,newTask).subscribe(success => {
        if (success) {
          this.currentTask=success
          this.edit()
        } else {
          console.log("Could not update task!");
        }
      });
    }
  }
  cancel(){
    this.edit()
    if(this.currentTask!=null){
      this.editForm.setValue(this.currentTask)
    }
    else
      this.leave()
  }

  leave() {
    if (this.route.snapshot.paramMap.get('pid'))
      this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('pid') + '/tasks']);
    else
      this.router.navigate(['/tasks']);
  }
}
