import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Worktime } from 'src/app/models/worktime.model';
import { TaskService } from 'src/app/services/task.service';
import { WorktimeService } from 'src/app/services/worktime.service';

interface Ssf {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-worktimes',
  templateUrl: './worktimes.component.html',
  styleUrls: ['./worktimes.component.css']
})
export class WorktimesComponent implements OnInit {
  worktimes: Worktime[] = [];
  @Input() archived
  //Assigning displayed columns
  displayedColumns: string[] = ['name','start_dt','user', 'time'];
  dataSource;
  @Input() id;
  selectedValue: string;
  selectedFilter: string | '';
  selectedSearch: string | null;
  filterVals: Ssf[] = [
    { value: 'all', viewValue: 'All' },
    { value: 'false', viewValue: 'Active' },
    { value: 'true', viewValue: 'Archived' }
  ];
  ssfForm: FormGroup;

  // getTime(start:Date,end:Date){
  //   if(end!=null){
  //   let startTime=new Date(start)
  //   let endTime=new Date(end)
  //   let time=endTime.getTime() - startTime.getTime()
  //   return (Math.round(time/60000))
  //   }else
  //     return 0
  // }

  constructor(public route: ActivatedRoute, private router: Router, private worktimeService: WorktimeService, public taskService: TaskService) {
    this.ssfForm = new FormGroup({
      filterOpts: new FormControl('all'),
      search: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.getWorktimes(this.id)

    this.route.queryParams.subscribe(params => {
      this.ssfForm.get('filterOpts').setValue(params.state);
      this.ssfForm.get('search').setValue(params.search);

      this.selectedValue = this.ssfForm.get('filterOpts').value;

      if (params.state == null) this.ssfForm.get('filterOpts').setValue('all');
      if (params.search == null) this.ssfForm.get('search').setValue('');
    });
  }

  applyFilter(column: string, filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  stateFilter() {
    var isFilterOk = false;
    this.selectedFilter = this.ssfForm.get('filterOpts').value?.toString();
    this.filterVals.forEach(v => {
      if (v.value == this.selectedFilter) isFilterOk = true;
    });
    this.selectedSearch = this.ssfForm.get('search').value?.toString().toLowerCase();
    if (isFilterOk) {
      this.selectedValue = this.selectedFilter;
      if (this.selectedSearch){
        this.worktimeService.ssfWorktimes("archived=" + this.selectedFilter + "&search=" + this.selectedSearch, this.id).subscribe(response => {
          if (response) {
            this.worktimes = response as Worktime[];
          } else {
            this.worktimes = Worktime[0];
          }
          this.initDataTable();
        });
      }else{
      this.worktimeService.ssfWorktimes("archived=" + this.selectedFilter, this.id).subscribe(response => {
        if (response) {
          this.worktimes = response as Worktime[];
        } else {
          this.worktimes = Worktime[0];
        }
        this.initDataTable();
      });
    }
   }
  }

  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.worktimes);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  @ViewChild(MatSort)
  sort: MatSort = new MatSort;


  @ViewChild(MatPaginator)
  paginator!: MatPaginator;


  newWorktime() {
    this.router.navigate(['/task/' + this.route.snapshot.paramMap.get('id') + '/worktime/' + 'new']);
  }

  edit(row: any) {
    if (this.route.snapshot.paramMap.get('id'))
      this.router.navigate(['/task/' + this.route.snapshot.paramMap.get('id') + '/worktime/' + row.id]);
  }

  getWorktimes(task_id:string) {
    this.worktimeService.getWorktimes(task_id).subscribe(success => {
      if (success) {
        this.worktimes = success
        this.initDataTable();
      } else {
        console.log("Couldnt load tasks")
      }
    });
  }
}
