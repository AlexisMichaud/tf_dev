import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Task } from 'src/app/models/task.model';
import { TaskService } from '../../../services/task.service';
import { TaskAnalytics } from '../../../models/taskAnalytics';
interface Ssf {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-task-analytic',
  templateUrl: './task-analytic.component.html',
  styleUrls: ['./task-analytic.component.css']
})
export class TaskAnalyticComponent implements OnInit {
  tasks: Task[] = [];
  //Assigning displayed columns
  displayedColumns: string[] = ['name', 'due_date', 'state','estimated_time','actual_time'];
  dataSource;
  id;
  selectedValue: string;
  selectedFilter: string | '';
  selectedSearch: string | null;
  filterVals: Ssf[] = [
    { value: 'all', viewValue: 'All' },
    { value: '0', viewValue: 'In Progress' },
    { value: '1', viewValue: 'Finished' },
    { value: '2', viewValue: 'Archived' },
  ];
  analyticsForm: FormGroup;
  analytics: TaskAnalytics
  ssfForm: FormGroup;
  taskState(state: number) {
    if (state == 0)
      return "In Progress"
    if (state == 1)
      return "Finished"
    if (state == 2)
      return "Archived"
    return null
  }

  constructor(private taskService:TaskService) {
    this.ssfForm = new FormGroup({
      filterOpts: new FormControl('all'),
      search: new FormControl('')
    });
    this.analyticsForm = new FormGroup({
      activePercent: new FormControl({ value: null, disabled: true }),
      percentOverTime: new FormControl({ value: null, disabled: true }),
      avgTime: new FormControl({ value: null, disabled: true }),
      worktimeCount: new FormControl({ value: null, disabled: true }),
      mostTime: new FormControl({ value: null, disabled: true }),
      mostTimeName: new FormControl({ value: null, disabled: true }),
      percentProblem: new FormControl({ value: null, disabled: true })
    });
   }

  applyFilter(column: string, filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;


  stateFilter() {
    var isFilterOk = false;
    this.selectedFilter = this.ssfForm.get('filterOpts').value?.toString();
    this.filterVals.forEach(v => {
      if (v.value == this.selectedFilter) isFilterOk = true;
    });

    if (isFilterOk) {
      this.selectedValue = this.selectedFilter;
      this.selectedSearch = this.ssfForm.get('search').value?.toString().toLowerCase();
      if (this.selectedSearch) {
        this.taskService.ssfTask("state=" + this.selectedFilter + "&search=" + this.selectedSearch).subscribe(response => {
          if(response){
          this.tasks = response as Task[];
          }else{
            this.tasks=Task[0]
          }
          this.initDataTable();
          });
        } else {
          this.taskService.ssfTask("state=" + this.selectedFilter).subscribe(response => {
            if(response){
            this.tasks = response as Task[];
            }else{
              this.tasks=Task[0]
            }
            this.initDataTable();
          });
        }
      }
    }
  ngOnInit(): void {
    this.getAllTasks()
    this.getAnalytics()
  }

  getAnalytics(){
    this.taskService.getAnalytics().subscribe(success => {
      if (success) {
        this.analytics = success
        if(this.analytics!=null){
        if(this.analytics.activePercent!=null){
          this.analyticsForm.get('activePercent').setValue(Math.floor(Number(this.analytics.activePercent))+"%")
        }else{
          this.analyticsForm.get('activePercent').setValue("N/A")
        }

        if(this.analytics.percentOverTime!=null){
          this.analyticsForm.get('percentOverTime').setValue(Math.floor(Number(this.analytics.percentOverTime))+"%")
        }else{
          this.analyticsForm.get('percentOverTime').setValue("N/A")
        }

        if(this.analytics.avgTime!=null){
          this.analyticsForm.get('avgTime').setValue(Math.floor(Number(this.analytics.avgTime))+" Mins")
        }else{
          this.analyticsForm.get('avgTime').setValue("N/A")
        }

        if(this.analytics.worktimeCount!=null){
          this.analyticsForm.get('worktimeCount').setValue(this.analytics.worktimeCount)
        }else{
          this.analyticsForm.get('worktimeCount').setValue("N/A")
        }

        if(this.analytics.mostTime!=null){
          this.analyticsForm.get('mostTime').setValue(Math.floor(Number(this.analytics.mostTime/60))+" Hours")
        }else{
          this.analyticsForm.get('mostTime').setValue("N/A")
        }

        if(this.analytics.mostTimeName!=null){
          this.analyticsForm.get('mostTimeName').setValue(this.analytics.mostTimeName)
        }else{
          this.analyticsForm.get('mostTimeName').setValue("N/A")
        }

        if(this.analytics.percentProblem!=null){
          this.analyticsForm.get('percentProblem').setValue(Math.floor(Number(this.analytics.percentProblem))+"%")
        }else{
          this.analyticsForm.get('percentProblem').setValue("N/A")
        }
      }else{
        this.analyticsForm.setValue({
          percentActive:"N/A",
          percentOvertime:"N/A",
          avgTime:"N/A",
          worktimeCount:"N/A",
          mostTime:"N/A",
          mostTimeName:"N/A",
          percentProblem:"N/A"
        })
      }
      } else {
        ("Couldnt load Analytics")
      }
    });
  }
  getAllTasks() {
    this.taskService.getAllTasks().subscribe(success => {
      if (success) {
        this.tasks = success
        this.initDataTable();
      } else {
        ("Couldnt load tasks")
      }
    });
  }
  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.tasks);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
