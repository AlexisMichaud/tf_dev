import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Team } from 'src/app/models/team.model';
import { AuthService } from 'src/app/services/auth.service';
import { TeamService } from 'src/app/services/team.service';
import { MembershipsService } from '../../../services/memberships.service';
import { DialogComponent } from '../../dialog/dialog.component';
import { Membership } from '../../../models/membership.model';
import { FormControl, FormGroup } from '@angular/forms';
import { Employee } from 'src/app/models/employee.model';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { array } from 'ts-json-object';

@Component({
  selector: 'app-team-edit-board-all-users',
  templateUrl: './team-edit-board-all-users.component.html',
  styleUrls: ['./team-edit-board-all-users.component.css']
})
export class TeamEditBoardAllUsersComponent implements OnInit {
  @Output() updateBoards = new EventEmitter();
  @Input() currentTeam!: Team
  AllEmployees: any[] = [];
  dataSource1: any;
  id;
  selectedSearch: string | null;
  ssfForm: FormGroup;
  addButton: boolean = false;
  newMembers:number[] = [];


  constructor(
    public authService: AuthService, 
    private router: Router, 
    private route: ActivatedRoute, 
    public teamService: TeamService,
    private dialog: MatDialog,
    public membershipsService: MembershipsService) {      this.ssfForm = new FormGroup({
      search: new FormControl('')
    }); }


    ngOnChanges(changes:SimpleChanges){
      //console.log("testchange",changes)
      this.updateBoadUser()
      
    }

  ngOnInit(): void {
    this.updateBoadUser()

    // this.route.queryParams.subscribe(params => {
    //   this.ssfForm.get('search').setValue(params.search);

    //   if (params.search == null) this.ssfForm.get('search').setValue('');
    // });

  }

  dataTable(){
    this.dataSource1 = new MatTableDataSource(this.AllEmployees);
    this.dataSource1.sort = this.sort;
    this.dataSource1.paginator = this.paginator;

  }
  updateBoadUser() {
    this.prepareSsfData();
      //this.router.navigate(['/team'], { queryParams: { search: this.selectedSearch } });
      this.membershipsService.ssfAllEmployees("&search=" + this.selectedSearch).subscribe(response => {
        if (response) {
          this.AllEmployees = response as Employee[];
          this.dataTable()
        }
        else
          this.AllEmployees = Employee[0];
          this.dataTable()

      });
    }

  private prepareSsfData() {
    this.selectedSearch = this.ssfForm.get('search').value?.toString().toLowerCase();

    if (this.selectedSearch.toString().trim().length < 1) this.selectedSearch = null;

  }


  displayedColumns1: string[] = ['firstname', 'lastname', 'email', 'phone'];

  applyFilter(filterValue: string) {
    this.dataSource1.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatSort) sort: MatSort = new MatSort;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  showOptions(event:MatCheckboxChange,userId: number): void {
    if(event.checked == true){
      this.newMembers.push(userId)
    }
    else{
    const index = this.newMembers.indexOf(userId);
    if(index > -1){
      this.newMembers.splice(index,1);
      } 
    }

    this.buttonAdd()
    //console.log(event.checked,userId);
    //console.log(this.newMembers)
  }

  buttonAdd(){
    if(this.newMembers.length >0){
      this.addButton = true
    }else
    this.addButton = false
  }

  addMember(){
    this.membershipsService.AddMember(this.newMembers,this.currentTeam.id).subscribe(success => {
      if (success) {
        this.valueChange()
        this.newMembers = []
        this.buttonAdd()
      } else {
        console.log("Could not add Member!");
      }
    });
  }

  valueChange(){
    this.updateBoards.emit('')
  }


}
