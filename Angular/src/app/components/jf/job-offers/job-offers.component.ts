import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JobOffer } from '../../../models/jobOffer.model';
import { JobOfferService } from '../../../services/job-offer.service';

interface Ssf {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-jobs',
  templateUrl: './job-offers.component.html',
  styleUrls: ['./job-offers.component.css']
})
export class JobOffersComponent implements OnInit {
  
  ssfForm: FormGroup;
  jobOffers: JobOffer[] = [];
  dataSource: any;


  jobOfferAvailability(value: number) {
    if (value == 0)
      return "Full Time"
    if (value == 1)
      return "Half Time"
    if (value == 2)
      return "Contractual"
    return null
  }


  displayedColumns: string[] = ['name', 'availability', 'nb_employee_looking_for'];
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(    
    public authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private jobOfferService: JobOfferService) 
    {
      this.ssfForm = new FormGroup({
        search: new FormControl('')
      });

     }

  ngOnInit(): void {

  }


  ngAfterViewInit() {
    this.refreshList();
  }

  select(row: any) {
    this.router.navigate(['/jobOffer', row.id]);
  }

  refreshList() {
      this.jobOfferService.ssfJobOffer("&search=" + this.getSelectedSearch()).subscribe(response => {
        if (this.authService.currentEmployee)
          if (response)
            this.jobOffers = response as JobOffer[];
          else
            this.jobOffers = JobOffer[0];

        this.initDataTable();
      });
    }



  private getSelectedSearch(): string {
    if (this.ssfForm.get('search').value?.toString().toLowerCase().trim().length < 1) return null;
    return this.ssfForm.get('search').value?.toString().toLowerCase();
  }


  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.jobOffers);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
