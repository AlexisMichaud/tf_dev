import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/models/team.model';
import { TeamAnalytics } from 'src/app/models/teamAnalytics.model';
import { JobOfferService } from 'src/app/services/job-offer.service';
import { TeamService } from '../../../services/team.service';

@Component({
  selector: 'app-team-analytics',
  templateUrl: './team-analytics.component.html',
  styleUrls: ['./team-analytics.component.css']
})
export class TeamAnalyticsComponent implements OnInit {
  currentTeam: Team
  selectForm: FormGroup;
  teamAnalytics: TeamAnalytics[] = [];
  dataSource: any;


  displayedColumns: string[] = ['name','firstname','lastname','numberOfMember','nbOfFullTime', 'nbOfHalfTime', 'nbOfContractual','age','numberOfPost'];
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  constructor(
    private route: ActivatedRoute,
    private jobOfferService: JobOfferService,
    private teamService: TeamService) {
      this.teamService.getTeam().subscribe(t => {
        this.currentTeam = t as Team
      });
      this.selectForm = new FormGroup({
        search: new FormControl('')
      });
    }
    ngAfterViewInit() {
      this.refreshList();
    }

    refreshList() {
      this.teamService.ssfTeamAnalytics("&search=" + this.getSelectedSearch()).subscribe(success => {
        if (success) {
          this.teamAnalytics = success
         
        } else {
          console.log('invalid')
        }
        this.initDataTable();
      });
    }
    private getSelectedSearch(): string {
      if (this.selectForm.get('search').value?.toString().toLowerCase().trim().length < 1) return null;
      return this.selectForm.get('search').value?.toString().toLowerCase();
    }

  ngOnInit(): void {
  }

  

  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.teamAnalytics);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }


  getTotalMember(){
    let totalMember=0
    this.teamAnalytics.forEach(t=>totalMember += t.numberOfMember)
    //console.log(totalMember)
    return totalMember
  }

  getNbOfOffers(){
    let totalOffers=0
    this.teamAnalytics.forEach(t=>totalOffers += t.numberOfPost)
    //console.log(totalOffers)
    return totalOffers
  }
  getNbOfFullTime(){
    let nbOfFullTime=0
    this.teamAnalytics.forEach(t=>nbOfFullTime += t.nbOfFullTime)
    //console.log(nbOfFullTime)
    return nbOfFullTime
  }
  getNbOfHalfTime(){
    let nbOfHalfTime=0
    this.teamAnalytics.forEach(t=>nbOfHalfTime += t.nbOfHalfTime)
    //console.log(nbOfHalfTime)
    return nbOfHalfTime
  }
  getNbOfContractual(){
    let nbOfContractual=0
    this.teamAnalytics.forEach(t=>nbOfContractual += t.nbOfContractual)
    //console.log(nbOfContractual)
    return nbOfContractual
  }

  changeAge(){
    let setAge = 0 
    this.teamAnalytics.map(t=>setAge = t.age)
    //let changeAge = DATEDIF(0,setAge,"y")&" years " &DATEDIF(0,setAge,"ym")&" months "&DATEDIF(0,setAge,"md")&" days"
  }

  getAverageAge(){
    let totalAge=0
    let count = this.teamAnalytics.length
    this.teamAnalytics.forEach(t=>(totalAge += t.age))
    
    return Math.round(totalAge /count)
  }



}
