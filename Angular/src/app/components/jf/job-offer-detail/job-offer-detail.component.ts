import { Component, OnInit } from '@angular/core';
import { JobOfferService } from '../../../services/job-offer.service';
import { JobOffer } from '../../../models/jobOffer.model';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-job-offer-detail',
  templateUrl: './job-offer-detail.component.html',
  styleUrls: ['./job-offer-detail.component.css']
})
export class JobOfferDetailComponent implements OnInit {
  currentJob: JobOffer;
  selectForm: FormGroup;

  constructor(    private router: Router,
    private route: ActivatedRoute,
    private jobOfferService: JobOfferService) 
    {

      this.selectForm = new FormGroup({
        name: new FormControl({ value: "", disabled: true }),
        description: new FormControl({ value: "", disabled: true }),
        availability: new FormControl({ value: "", disabled: true }),
        nb_employee_looking_for: new FormControl({ value: "", disabled: true })
      });


      this.findJobOffer()
    }

  ngOnInit(): void {
  }


  findJobOffer() {
    //console.log("testfindteam")
    this.jobOfferService.getJobOffer(this.route.snapshot.paramMap.get('id')).subscribe(success => {
      if (success) {
        this.currentJob = success as JobOffer

        this.selectForm.controls['name'].setValue(this.currentJob.name)
        this.selectForm.controls['description'].setValue(this.currentJob.description)
        this.selectForm.controls['availability'].setValue(this.jobOfferAvailability(this.currentJob.availability))
        this.selectForm.controls['nb_employee_looking_for'].setValue(this.currentJob.nb_employee_looking_for)
        //console.log(this.currentTeam)
      } else {
        console.log('invalid')
      }
    });
  }


  jobOfferAvailability(value: number) {
    if (value == 0)
      return "Full Time"
    if (value == 1)
      return "Half Time"
    if (value == 2)
      return "Contractual"
    return null
  }



}
