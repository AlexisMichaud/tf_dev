import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocationService } from 'src/app/services/location.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-new-team-component',
  templateUrl: './new-team-component.html',
  styleUrls: ['./new-team-component.css']
})
export class NewTeamComponent implements OnInit {
  editForm: FormGroup;



  constructor(
    public authService: AuthService,    
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private location: LocationService,
    public teamService: TeamService)  
    {
    this.editForm = new FormGroup({
      name: new FormControl( null , [Validators.required, Validators.maxLength(45)]),
      description: new FormControl(null , [Validators.required, Validators.maxLength(255)]), 
      });
   }


  ngOnInit(): void {}


  create(){
      let newTeam = this.editForm.value
      this.teamService.createTeam(newTeam).subscribe(success => {
        if (success) {
          this.editForm.reset()
          this.router.navigate(['/team']);
        } else {
          console.log("Could not create team!");
          this.router.navigate(['/team']);
        }
      });

    }

  cancel(){
    this.location.goBack();
  }

}
