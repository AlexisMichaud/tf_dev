import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { MatSort } from '@angular/material/sort'
import { MatPaginator } from '@angular/material/paginator'
import { MatTableDataSource } from '@angular/material/table'
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TeamService } from '../../../services/team.service';
import { DialogComponent } from '../../dialog/dialog.component';
import { LocationService } from 'src/app/services/location.service';
import { Team } from 'src/app/models/team.model';
import { Employee } from '../../../models/employee.model';
import { TeamBoardMembersComponent } from '../team-board-members/team-board-members.component';
import { TeamEditBoardAllUsersComponent } from '../team-edit-board-all-users/team-edit-board-all-users.component';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  editForm: FormGroup;
  editToggle: boolean = true;
  editMember: boolean = true;
  currentTeam: Team = undefined;
  currentDate = new Date() 
  @ViewChild('boardMembers') boardMemberComponent: TeamBoardMembersComponent;
  @ViewChild('boardAllUsers') bordAllUserComonent: TeamEditBoardAllUsersComponent;


  id!: string | null;

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    public teamService: TeamService) {
    this.editForm = new FormGroup({
      name: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(45)]),
      description: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.maxLength(255)]),
      message: new FormControl({ value: null, disabled: true }),
      message_expiration: new FormControl({ value: null, disabled: true })

    },this.messageValidator());
    this.findTeam()
  }

  ngOnInit(): void {

  }


  messageValidator(): ValidatorFn {
    return (form: AbstractControl): { [key: string]: boolean } | null => {
      //console.log("editfrom value expiration", this.editForm.get("message_expiration")?.value)
      //console.log("editfrom value message", this.editForm.get("message")?.value)
     let message = this.editForm.get("message")?.value;
     let messageExp = this.editForm.get("message_expiration")?.value

     if(( message =='' && messageExp == null)||(message !='' && messageExp != null)) {
      return null


     }
     
     else{
      //console.log("invalid")
      return {message:true}

     }
    };
  }



  findTeam() {
    //console.log("testfindteam")

    this.teamService.getTeam().subscribe(success => {
      if (success) {
        this.currentTeam = success as Team
        //console.log(this.currentTeam)
        this.editForm.controls['name'].setValue(this.currentTeam.name)
        this.editForm.controls['description'].setValue(this.currentTeam.description)
        this.editForm.controls['message'].setValue(this.currentTeam.message)
        this.editForm.controls['message_expiration'].setValue(this.currentTeam.message_expiration)
        //console.log(this.currentTeam)
      } else {
        //console.log(this.currentTeam)
        this.currentTeam = null
        //console.log(this.currentTeam)
      }
    });
  }


  confirm() {
    let teamInfo = this.editForm.value
    teamInfo.id = this.currentTeam.id
    this.teamService.updateTeam(teamInfo).subscribe(success => {
      if (success) {
        //console.log("Team updated")
        this.disable();
      } else {
        console.log("Could not update team!");
      }
    });
  }


  openDialog() {
    let dialogRef;
    dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'Confirm deletion?',
        message: 'Are you sure you wish to delete this team, all members, jobOffer, project and task associated?',
        true: 'Yes',
        false: 'Cancel'
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result== 'true') {
        this.teamService.deleteTeam(this.currentTeam.id.toString()).subscribe(success => {
          if (success) {
            this.router.navigate(['/home'])
          } else {
            console.log("Could not delete team!");
          }
        });
      }
    })
  }

  updateAllBoard(){
    this.boardMemberComponent.updateBoardMember()
    this.bordAllUserComonent.updateBoadUser()
  }

  editAllMember(){
    this.editMember = !this.editMember;
  }

  cancel() {
    const {id,owner_id, ...teamInfo} = this.currentTeam
    this.editForm.get('name')?.disable();
    this.editForm.get('description')?.disable();
    this.editForm.get('message_expiration')?.disable();
    this.editForm.get('message')?.disable();
    this.editToggle = !this.editToggle;
    this.editForm.setValue(teamInfo)
  }

  disable() {
    this.editForm.get('name')?.disable();
    this.editForm.get('description')?.disable();
    this.editForm.get('message_expiration')?.disable();
    this.editForm.get('message')?.disable();
    this.editToggle = !this.editToggle;
  }
  edit() {
    this.editForm.get('name')?.enable();
    this.editForm.get('description')?.enable();
    this.editForm.get('message')?.enable();
    this.editForm.get('message_expiration')?.enable();
    this.editToggle = !this.editToggle;
  }



}
