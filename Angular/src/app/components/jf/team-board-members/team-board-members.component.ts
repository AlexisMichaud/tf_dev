import { Component, EventEmitter, Input, OnInit, Output, SimpleChange, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TeamComponent } from '../team/team.component';
import { Team } from '../../../models/team.model';
import { TeamService } from 'src/app/services/team.service';
import { Employee } from '../../../models/employee.model';
import { DialogComponent } from '../../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MembershipsService } from 'src/app/services/memberships.service';
import { ResourceLoader } from '@angular/compiler';
import { FormControl, FormGroup } from '@angular/forms';
import { Membership } from '../../../models/membership.model';
interface Ssf {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-team-board-members',
  templateUrl: './team-board-members.component.html',
  styleUrls: ['./team-board-members.component.css']
})
export class TeamBoardMembersComponent implements OnInit {

  @Output() updateBoards = new EventEmitter(); 
  @Input() currentTeam!: Team
  @Input() editMember!: boolean;
  memberships: any[] = [];
  dataSource: any;
  id;
  selectedValue: string;
  selectedFilter: string | '';
  selectedSearch: string | null;

  filterVals: Ssf[] = [
    { value: 'all', viewValue: 'All' },
    { value: '0', viewValue: 'FullTime' },
    { value: '1', viewValue: 'HalfTime' },
    { value: '2', viewValue: 'Contractual' },
  ];

  ssfForm: FormGroup;
  ngOnChanges(changes:SimpleChanges){
    //console.log("testchange",changes)
    this.updateBoardMember()
    
  }

  constructor(
    public authService: AuthService, 
    private router: Router, 
    private route: ActivatedRoute, 
    public teamService: TeamService,
    private dialog: MatDialog,
    public membershipsService: MembershipsService) {
      this.ssfForm = new FormGroup({
        filterOpts: new FormControl('all'),
        search: new FormControl('')
      });
     }

  ngOnInit(): void {
    this.updateBoardMember()

    
    this.route.queryParams.subscribe(params => {
      //console.log("subscribe",params)
      
      this.ssfForm.get('search').setValue(params.search);

      if (params.state == null) this.ssfForm.get('filterOpts').setValue('all');
      else{
        this.ssfForm.get('filterOpts').setValue(params.state);
      }
      if (params.search == null) this.ssfForm.get('search').setValue('');

      this.selectedValue = this.ssfForm.get('filterOpts').value;
      //console.log("subscribe selected",this.selectedValue)
    });

  }
  displayedColumns: string[] = ['firstname', 'lastname', 'email', 'phone'];

  applyFilter(column: string, filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  private prepareSsfData() {
    this.selectedFilter = this.ssfForm.get('filterOpts').value?.toString().toLowerCase();
    this.selectedSearch = this.ssfForm.get('search').value?.toString().toLowerCase();

    if (this.selectedSearch.toString().trim().length < 1) this.selectedSearch = null;

  }
  private isValueInFilterVals(value: string): boolean {
    var isFilterOk = false;
    this.filterVals.forEach(v => { if (v.value == value) isFilterOk = true; });
    return isFilterOk;
  }

  private datatable(){          
    this.dataSource = new MatTableDataSource(this.memberships);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

  }

  updateBoardMember() {
    this.prepareSsfData();
    //console.log(this.selectedFilter)
    //console.log(this.isValueInFilterVals(this.selectedFilter))
    if (this.isValueInFilterVals(this.selectedFilter)) {

      this.ssfForm.get('filterOpts').setValue(this.selectedFilter)
      this.router.navigate(['/team'], { queryParams: { state: this.selectedFilter, search: this.selectedSearch } });
       this.teamService.ssfMembers("availability=" + this.selectedFilter +"&search=" + this.selectedSearch).subscribe(response => {
         if (response) {
           this.memberships = response as Membership[];
         this.datatable()

         }
         else
           this.memberships = Membership[0];
         this.datatable()

       });
    }
  }


  @ViewChild(MatSort) sort: MatSort = new MatSort;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  delete(row: any) {
    this.openDialog(row)

  }
  openDialog(row) {

    let dialogRef;
    dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm  Member deletion?',
          message: 'Are you sure you wish to delete this Member to your team',
          true: 'Yes',
          false: 'Cancel'
        }
      }); 
    dialogRef.afterClosed().subscribe((result) => {
      //console.log(row.id)
      if (result == 'true') {
        this.membershipsService.deleteMember(row.id).subscribe(success => {
          if (success) {
            this.valueChange();
          } else {
            console.log("Could not delete Member!");
          }
        });
      }
    })
  }


  valueChange(){

    this.updateBoards.emit('')

  }


}
