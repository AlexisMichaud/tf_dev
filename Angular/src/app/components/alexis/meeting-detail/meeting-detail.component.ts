import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { DialogComponent } from "../../dialog/dialog.component";
import { LocationService } from "src/app/services/location.service";
import { Meeting } from "src/app/models/meeting.model";
import { ProjectService } from "src/app/services/project.service";
import { MeetingService } from '../../../services/meeting.service';
import { MatSnackBar } from "@angular/material/snack-bar";
import Response from 'src/app/models/response.model';

interface CustomProject {
  id: number;
  name: string;
  archived: boolean;
}

@Component({
  selector: 'app-meeting-detail',
  templateUrl: './meeting-detail.component.html',
  styleUrls: ['./meeting-detail.component.css']
})
export class MeetingDetailComponent implements OnInit {
  editForm: FormGroup;
  fullCurrentMeeting: Meeting;
  currentProject: CustomProject;
  editToggle: boolean = false;
  minDate = new Date();

  constructor(
    public route: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private location: LocationService,
    private meetingService: MeetingService,
    private projectService: ProjectService,
    private snackBar: MatSnackBar) {
    this.editToggle = this.route.snapshot.paramMap.get('id').startsWith('new') ? true : false;
    if (this.editToggle) {
      this.currentProject = {
        name: "Project",
        id: +this.route.snapshot.url[1],
        archived: false
      }
      this.editForm = new FormGroup({
        name: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
        summary: new FormControl(null, [Validators.required, Validators.maxLength(255)]),
        location: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
        start_dt: new FormControl(null, [Validators.required]),
        end_dt: new FormControl(null, [Validators.required])
      });
    } else {
      this.projectService.projectDetail(+this.route.snapshot.url[1]).subscribe(success => {
        if (success.success) {
          this.currentProject = {
            name: success.data.name,
            id: success.data.id,
            archived: success.data.archived
          }
        } else {
          this.router.navigate(['projects']);
        }
      });

      this.editForm = new FormGroup({
        name: new FormControl({ value: "", disabled: true }, [Validators.required, Validators.maxLength(45)]),
        summary: new FormControl({ value: "", disabled: true }, [Validators.required, Validators.maxLength(255)]),
        location: new FormControl({ value: "", disabled: true }, [Validators.required, Validators.maxLength(45)]),
        start_dt: new FormControl({ value: "", disabled: true }, [Validators.required]),
        end_dt: new FormControl({ value: "", disabled: true }, [Validators.required])
      });
      this.meetingService.meetingDetail(+this.route.snapshot.paramMap.get('id')).subscribe((success) => {
        if (success.success) {
          this.fullCurrentMeeting = success.data as Meeting;

          const { name, summary, location, start_dt, end_dt } = this.fullCurrentMeeting;
          this.editForm.setValue({ name, summary, location, start_dt, end_dt });
        } else {
          this.handleError(success);
        }
      });
    }
  }

  ngOnInit(): void {
  }

  edit() {
    if (this.route.snapshot.paramMap.get('id').valueOf() == 'new') {
      this.location.goBack();
    } else {
      if (!this.editToggle) {
        this.editForm.get('name')?.enable();
        this.editForm.get('summary')?.enable();
        this.editForm.get('location')?.enable();
        this.editForm.get('start_dt')?.enable();
        this.editForm.get('end_dt')?.enable();
      }
      else {
        const { name, summary, location, start_dt, end_dt } = this.fullCurrentMeeting;
        this.editForm.setValue({ name, summary, location, start_dt, end_dt });
        this.editForm.get('name')?.disable();
        this.editForm.get('summary')?.disable();
        this.editForm.get('location')?.disable();
        this.editForm.get('start_dt')?.disable();
        this.editForm.get('end_dt')?.disable();
      }

      this.editToggle = !this.editToggle;
    }
  }

  submit() {
    if (this.route.snapshot.paramMap.get('id').valueOf() == 'new') {
      this.meetingService.create(
        new Meeting({
          name: this.editForm.get('name').value,
          summary: this.editForm.get('summary').value,
          location: this.editForm.get('location').value,
          start_dt: this.editForm.get('start_dt').value,
          end_dt: this.editForm.get('end_dt').value,
          project_id: this.currentProject.id
        })).subscribe(success => {
          if (success.success) {
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.navigate(['/project/' + this.route.snapshot.url[1] + '/meeting', success.data.id]);
          } else {
            this.handleError(success);
          }
        });
    } else {
      this.meetingService.update(
        new Meeting({
          name: this.editForm.get('name').value,
          summary: this.editForm.get('summary').value,
          location: this.editForm.get('location').value,
          start_dt: this.editForm.get('start_dt').value,
          end_dt: this.editForm.get('end_dt').value,
          id: this.fullCurrentMeeting.id,
          project_id: this.fullCurrentMeeting.project_id
        })
      ).subscribe(success => {
        if (success.success) {
          this.fullCurrentMeeting = success.data as Meeting;

          const { name, summary, location, start_dt, end_dt } = this.fullCurrentMeeting;
          this.editForm.setValue({ name, summary, location, start_dt, end_dt });

          this.edit();
        } else {
          this.handleError(success);
        }
      });
    }
  }

  openDialog(action: string) {
    let dialogRef;
    if (action == 'delete') {
      dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm deletion?',
          message: 'Are you sure you wish to delete this meeting?',
          true: 'Yes',
          false: 'Cancel'
        }
      });
    } else {
      dialogRef = this.dialog.open(DialogComponent, {
        data: {
          title: 'Confirm archiving/unarchiving?',
          message: `Are you sure you wish to archive/unarchive this meeting?
                    Once archived, you will no longer be able to edit it.`,
          true: 'Yes',
          false: 'Cancel'
        }
      });
    }

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'true') {
        switch (action) {
          case 'delete':
            this.meetingService.delete(this.fullCurrentMeeting.id).subscribe(success => {
              if (success) {
                console.log("OK ", success);
                this.router.navigate(['/project/' + this.route.snapshot.url[1]]);
              } else {
                console.log("Error ", success);
                this.handleError(success);
              }
            });
            break;

          case 'archive':
            this.meetingService.archive(
              new Meeting({
                name: this.editForm.get('name').value,
                summary: this.editForm.get('summary').value,
                location: this.editForm.get('location').value,
                start_dt: this.editForm.get('start_dt').value,
                end_dt: this.editForm.get('end_dt').value,
                id: this.fullCurrentMeeting.id
              })
            ).subscribe(success => {
              if (success.success) {
                this.fullCurrentMeeting.archived = success.data.archived;
                this.router.navigate([`/project/${this.currentProject.id}/meeting/${this.fullCurrentMeeting.id}`]);
              } else {
                console.log("Error ", success);
                this.handleError(success);
              }
            });
            break;

          default:
            break;
        }
      }
    })
  }

  private handleError(res: Response<Meeting> | Response<Meeting[]>) {
    if ('data' in res) {
      if (typeof res.data === 'string') {
        this.snackBar.open(res.data);
      } else {
        for (const field in res.data) {
          const errors = res.data;
          for (const error in errors) {
            this.snackBar.open(`Invalid action.`, 'Dismiss');
          }
        }
      }
    } else {
      this.snackBar.open('An error occurred.', 'Dismiss');
    }
  }
}
