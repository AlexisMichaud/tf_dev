import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { ProjectAnalytics } from "src/app/models/project-analytics.model";
import { AuthService } from "src/app/services/auth.service";
import { ProjectService } from "src/app/services/project.service";
import { Filter } from '../../../models/filter.model'

@Component({
  selector: 'app-projects-analytics',
  templateUrl: './projects-analytics.component.html',
  styleUrls: ['./projects-analytics.component.css']
})
export class ProjectsAnalyticsComponent implements OnInit {
  ssfForm: FormGroup;
  projects: ProjectAnalytics[] = [];
  stats: ProjectAnalytics;
  dataSource: any;
  filterVals: Filter[] = [
    { value: 'all', viewValue: 'All' },
    { value: 'active', viewValue: 'Active' },
    { value: 'archived', viewValue: 'Archived' }
    // { value: 'completed', viewValue: 'Completed' }
  ];
  selectedDateRange: Date[] = [
    new Date(),
    new Date()
  ];
  displayedColumns: string[] = ['name', 'archived', 'meetings_count'];
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(public authService: AuthService, private router: Router, private projectService: ProjectService, private route: ActivatedRoute) {
    var dateInitializer = new Date();
    this.selectedDateRange[0].setFullYear(dateInitializer.getFullYear());
    dateInitializer.setFullYear(dateInitializer.getFullYear() + 1);
    this.selectedDateRange[1].setFullYear(dateInitializer.getFullYear());

    this.ssfForm = new FormGroup({
      filterOpts: new FormControl('all'),
      search: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (this.isValueInFilterVals(params.status))
          this.ssfForm.get('filterOpts').setValue(params.status);
        else
          this.router.navigate(['/projects/analytics'], { queryParams: { status: 'all' } });

        if (params.status == null) this.ssfForm.get('filterOpts').setValue('all');
        if (params.search != null) this.ssfForm.get('search').setValue(params.search);
      });
  }

  ngAfterViewInit() {
    this.refreshList();
  }

  edit(row: any) {
    this.router.navigate([`/project/${row.id}`], { queryParams: { status: 'all' } });
  }

  applyFilter(filterValue: string) {
    console.log(filterValue)
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  refreshList() {
    if (this.isValueInFilterVals(this.getSelectedFilter())) {
      var date_range =
        this.selectedDateRange.includes(null) ?
          null :
          (this.getFormattedSelectedDateRange()[0] +
            "to" +
            this.getFormattedSelectedDateRange()[1])

      this.router.navigate(['/projects/analytics'], {
        queryParams: { status: this.getSelectedFilter(), search: this.getSelectedSearch(), date_range: date_range }
      });

      this.projectService.analytics(
        "status=" + this.getSelectedFilter() + "&search=" + this.getSelectedSearch() + "&date_range=" + date_range)
        .subscribe(response => {
          if (response && response.success) {
            this.stats = response.data.shift();
            this.projects = response.data as ProjectAnalytics[];
          }
          else
            this.projects = ProjectAnalytics[0];

          this.initDataTable();
        });
    }
    else
      this.router.navigate(['/projects'], { queryParams: { status: 'none' } });
  }

  toStatus(val: string): string {
    return val ? 'Archived' : 'Active';
  }

  mostMeetings(): number {
    var best = 0;
    this.projects.forEach(p => best < p.meetings_count ? best = p.meetings_count : 'nothing happens')
    return best;
  }

  clearDateRange() {
    this.selectedDateRange = [null, null];
    this.refreshList();
  }

  getFormattedSelectedDateRange(): string[] {
    if (this.selectedDateRange.includes(null))
      return [null, null];

    var formatted: string[] = [];

    var date = this.selectedDateRange[0].toLocaleDateString().split('/');
    formatted[0] = date[2] + '-' + date[0] + '-' + date[1];

    date = this.selectedDateRange[1].toLocaleDateString().split('/');
    formatted[1] = date[2] + '-' + date[0] + '-' + date[1];

    return formatted;
  }

  getFloatToTime(): number[] {
    var hours: number, minutes: number;
    if (this.stats?.avg_meeting_time != null) {
      hours = +this.stats.avg_meeting_time.toString().split('.')[0];
      minutes = +(this.stats.avg_meeting_time.toString().split('.')[1] + '0');

      minutes = ((minutes * 60) / 100);
      return [hours, +minutes.toString().slice(0, 2)];
    }
    return [0, 0];
  }

  private getSelectedFilter(): string {
    return this.ssfForm.get('filterOpts').value?.toString().toLowerCase();
  }

  private getSelectedSearch(): string {
    var search = this.ssfForm.get('search').value?.toString().toLowerCase();
    return search.trim().length < 1 ? null : search;
  }

  private isValueInFilterVals(value: string): boolean {
    var isFilterOk = false;
    this.filterVals.forEach(v => { if (v.value == value) isFilterOk = true; });
    return isFilterOk;
  }

  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.projects);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
