import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from "src/app/services/auth.service";
import { MatSort } from '@angular/material/sort'
import { MatPaginator } from '@angular/material/paginator'
import { MatTableDataSource } from '@angular/material/table'
import { Router } from "@angular/router";
import { ProjectService } from "src/app/services/project.service";
import { Project } from "src/app/models/project.model";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import { TeamService } from "src/app/services/team.service";
import { Team } from 'src/app/models/team.model';
import { Filter } from '../../../models/filter.model'


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
})

export class ProjectsComponent implements OnInit {
  currentTeam: Team
  ssfForm: FormGroup;
  projects: Project[] = [];
  dataSource: any;
  filterVals: Filter[] = [
    { value: 'all', viewValue: 'All' },
    { value: 'active', viewValue: 'Active' },
    { value: 'archived', viewValue: 'Archived' }
  ];
  displayedColumns: string[] = ['name', 'description', 'due_date'];
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public authService: AuthService,
    private router: Router,
    private projectService: ProjectService,
    private route: ActivatedRoute,
    public teamService: TeamService) {
    this.teamService.getTeam().subscribe(t => {
      this.currentTeam = t as Team
    });

    this.ssfForm = new FormGroup({
      filterOpts: new FormControl('all'),
      search: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (this.isValueInFilterVals(params.status))
          this.ssfForm.get('filterOpts').setValue(params.status);
        else
          this.router.navigate(['/projects'], { queryParams: { status: 'all' } });

        if (params.status == null) this.ssfForm.get('filterOpts').setValue('all');
        if (params.search != null) this.ssfForm.get('search').setValue(params.search);
      });
  }

  ngAfterViewInit() {
    this.refreshList();
  }

  edit(row: any) {
    this.router.navigate([`/project/${row.id}`], { queryParams: { status: 'all' } });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  refreshList() {
    if (this.isValueInFilterVals(this.getSelectedFilter())) {
      this.router.navigate(['/projects'], { queryParams: { status: this.getSelectedFilter(), search: this.getSelectedSearch() } });

      this.projectService.ssfProjects("status=" + this.getSelectedFilter() + "&search=" + this.getSelectedSearch()).subscribe(response => {
        if (this.authService.currentEmployee)
          if (response && response.success)
            this.projects = response.data as Project[];
          else
            this.projects = Project[0];

        this.initDataTable();
      });
    }
    else
      this.router.navigate(['/projects'], { queryParams: { status: 'none' } });
  }

  private getSelectedFilter(): string {
    return this.ssfForm.get('filterOpts').value?.toString().toLowerCase();
  }

  private getSelectedSearch(): string {
    if (this.ssfForm.get('search').value?.toString().toLowerCase().trim().length < 1) return null;
    return this.ssfForm.get('search').value?.toString().toLowerCase();
  }

  private isValueInFilterVals(value: string): boolean {
    var isFilterOk = false;
    this.filterVals.forEach(v => { if (v.value == value) isFilterOk = true; });
    return isFilterOk;
  }

  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.projects);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
}
