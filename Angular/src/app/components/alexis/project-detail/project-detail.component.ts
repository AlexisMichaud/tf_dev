import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort'
import { MatPaginator } from '@angular/material/paginator'
import { MatTableDataSource } from '@angular/material/table'
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { DialogComponent } from '../../dialog/dialog.component'
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ProjectService } from "src/app/services/project.service";
import { Project } from "src/app/models/project.model";
import { Meeting } from "src/app/models/meeting.model";
import { MeetingService } from "src/app/services/meeting.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import Response from 'src/app/models/response.model';
import { Location, PlatformLocation } from "@angular/common";

interface Ssf {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css'],
})
export class ProjectDetailComponent implements OnInit {
  ssfForm: FormGroup;
  editForm: FormGroup;
  editToggle: boolean = false;
  minDate = new Date();
  currentProject: Project;
  fullCurrentProject: Project;
  meetings: Meeting[] = [];
  dataSource: any;
  filterVals: Ssf[] = [
    { value: 'all', viewValue: 'All' },
    { value: 'active', viewValue: 'Active' },
    { value: 'archived', viewValue: 'Archived' }
  ];
  displayedColumns: string[] = ['name', 'summary', 'location'];
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    public route: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    public projectService: ProjectService,
    public meetingService: MeetingService,
    private snackBar: MatSnackBar) {
    this.minDate.setHours(0, 0, 0)
    this.ssfForm = new FormGroup({
      filterOpts: new FormControl('all'),
      search: new FormControl('')
    });

    this.editToggle = this.route.snapshot.paramMap.get('id').startsWith('new') ? true : false;
    if (this.editToggle) {
      this.editForm = new FormGroup({
        name: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
        description: new FormControl(null, [Validators.required, Validators.maxLength(255)]),
        due_date: new FormControl(null, [Validators.required])
      });
    } else {
      this.editForm = new FormGroup({
        name: new FormControl({ value: "", disabled: true }, [Validators.required]),
        description: new FormControl({ value: "", disabled: true }, [Validators.required]),
        due_date: new FormControl({ value: "", disabled: true }, [Validators.required])
      });
      this.projectService.projectDetail(+this.route.snapshot.paramMap.get('id')).subscribe(success => {
        if (success.success) {
          this.fullCurrentProject = success.data as Project;

          const { name, description, due_date } = this.fullCurrentProject;
          this.editForm.setValue({ name, description, due_date });
        } else {
          this.handleError(success);
          this.router.navigate(['projects']);
        }
      });
    }
  }

  ngOnInit(): void {
    if (!this.editToggle) {
      this.route.queryParams
        .subscribe(params => {
          if (this.isValueInFilterVals(params.status))
            this.ssfForm.get('filterOpts').setValue(params.status);
          else
            this.router.navigate([`/project/${this.fullCurrentProject?.id}`], { queryParams: { status: 'all' } });

          if (params.status == null) this.ssfForm.get('filterOpts').setValue('all');
          if (params.search != null) this.ssfForm.get('search').setValue(params.search);
        });
    }
  }

  ngAfterViewInit() {
    this.refreshList();
  }

  editMeeting(row: any) {
    this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('id') + '/meeting', row.id]);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  refreshList() {
    if (!this.editToggle && this.isValueInFilterVals(this.getSelectedFilter())) {
      this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('id')], { queryParams: { status: this.getSelectedFilter(), search: this.getSelectedSearch() } });

      this.meetingService.ssfMeetings("project=" + this.route.snapshot.paramMap.get('id') + "&status=" + this.getSelectedFilter() + "&search=" + this.getSelectedSearch()).subscribe(response => {
        if (response && response.success)
          this.meetings = response.data as Meeting[];
        else
          this.meetings = Meeting[0];

        this.initDataTable();
      });
    }
    else
      this.router.navigate(['/project/' + this.route.snapshot.paramMap.get('id')], { queryParams: { status: 'none' } });
  }

  edit() {
    if (this.route.snapshot.paramMap.get('id').valueOf() == 'new') {
      this.router.navigate(['projects'])
    } else {
      if (!this.editToggle) {
        this.editForm.get('name')?.enable();
        this.editForm.get('description')?.enable();
        this.editForm.get('due_date')?.enable();
      } else {
        const { name, description, due_date } = this.fullCurrentProject;
        this.editForm.setValue({ name, description, due_date });
        this.editForm.get('name')?.disable();
        this.editForm.get('description')?.disable();
        this.editForm.get('due_date')?.disable();
      }
      this.editToggle = !this.editToggle;
    }
  }

  submit() {
    if (this.route.snapshot.paramMap.get('id').valueOf() == 'new') {
      this.projectService.create(this.editForm.value).subscribe(success => {
        if (success.success) {
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.navigate(['/project', success.data.id]);
        } else {
          this.handleError(success);
        }
      });
    } else {
      this.projectService.update(
        new Project({
          name: this.editForm.get('name').value,
          description: this.editForm.get('description').value,
          due_date: this.editForm.get('due_date').value,
          id: this.fullCurrentProject.id,
          archived: this.fullCurrentProject.archived
        })
      ).subscribe(success => {
        if (success.success) {
          this.fullCurrentProject = success.data as Project;

          const { name, description, due_date } = this.fullCurrentProject;
          this.editForm.setValue({ name, description, due_date });

          this.edit();
        } else {
          this.handleError(success);
        }
      });
    }
  }

  openDialog(action: string) {
    let dialogRef: MatDialogRef<DialogComponent, any>;
    if (action == 'delete') {
      dialogRef = this.dialog.open(DialogComponent, {
        autoFocus: false,
        data: {
          title: 'Confirm deletion?',
          message: `Are you sure you wish to permanently delete this project and all its associated elements (meetings, tasks, worktimes)?`,
          true: 'Yes',
          false: 'Cancel'
        }
      });
    } else {
      dialogRef = this.dialog.open(DialogComponent, {
        autoFocus: false,
        data: {
          title: 'Confirm archiving/unarchiving?',
          message: `Are you sure you wish to archive/unarchive this project and all its associated elements (meetings, tasks, worktimes)?
                    Once archived, you will no longer be able to edit it nor any of its associated elements.`,
          true: 'Yes',
          false: 'Cancel'
        }
      });
    }

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'true') {
        switch (action) {
          case 'delete':
            this.projectService.delete(this.fullCurrentProject.id).subscribe(success => {
              if (success)
                this.router.navigate(['/projects']);
              else {
                console.log("Error ", success);
                this.handleError(success);
              }
            });
            break;

          case 'archive':
            this.projectService.archive(
              new Project({
                name: this.editForm.get('name').value,
                description: this.editForm.get('description').value,
                due_date: this.editForm.get('due_date').value,
                id: this.fullCurrentProject.id
              })
            ).subscribe(success => {
              if (success.success) {
                this.fullCurrentProject.archived = success.data.archived;
                this.refreshList();
              } else {
                console.log("Error ", success);
                this.handleError(success);
              }
            });
            break;

          default:
            break;
        }
      }
    })
  }

  private getSelectedFilter(): string {
    return this.ssfForm.get('filterOpts').value?.toString().toLowerCase();
  }

  private getSelectedSearch(): string {
    if (this.ssfForm.get('search').value?.toString().toLowerCase().trim().length < 1) return null;
    return this.ssfForm.get('search').value?.toString().toLowerCase();
  }

  private isValueInFilterVals(value: string): boolean {
    var isFilterOk = false;
    this.filterVals.forEach(v => { if (v.value == value) isFilterOk = true; });
    return isFilterOk;
  }

  private initDataTable() {
    this.dataSource = new MatTableDataSource(this.meetings);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  private handleError(res: Response<Project> | Response<Project[]>) {
    if ('data' in res) {
      if (typeof res.data === 'string') {
        this.snackBar.open(res.data);
      } else {
        for (const field in res.data) {
          const errors = res.data;
          for (const error in errors) {
            this.snackBar.open(`${field}: ${error}`, 'Dismiss');
          }
        }
      }
    } else {
      this.snackBar.open('An error occurred.', 'Dismiss');
    }
  }
}
