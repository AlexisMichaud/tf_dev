import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Team } from '../models/team.model';
import { AuthService } from './auth.service';
import { Employee } from '../models/employee.model';
import { TeamAnalytics } from '../models/teamAnalytics.model';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  getTeam(): Observable<Team | null | boolean> {
    return this.http.get<any>('/api/teams.json').pipe(
      map(response => {
        //console.log('getTeams', response);
        if (response.data) {
          //console.log(response.data)
          const { created_at, updated_at, ...TeamInfo } = response.data
          //console.log(TeamInfo)
          let newTeam = new Team(TeamInfo)
          //console.log(response)
          return newTeam;
        } else {
          return false;
        }
      }),
      catchError(error => {
        //console.log('Error', error);

        return of(false);
      })
    );
  }

  getMember(): Observable<any[] | null> {
    return this.http.get<any>('/api/teams/0/member.json').pipe(
      map(response => {
        if (response.data) {
          return response.data;
        } else {
          return false;
        }
      }),
      catchError(error => {
        console.log('Error', error);

        return of(false);
      })
    );
  }

  ssfMembers(params: string): Observable<[]> {
    //console.log("params", params)
    return this.http.get<any>('/api/teams/0/member.json?' + params).pipe(
      map(response => {
        //console.log("response", response)
        if (response.success && response.data && response.data.length >= 1)

          return response.data;

        return null;
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    )
  }


  constructor(private http: HttpClient, private authService: AuthService) { }


  createTeam(team: Team): Observable<boolean> {
    return this.http.post<any>('/api/teams', team).pipe(
      map(result => {
        //console.log('New team: ', result);
        return result.data;
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    );
  }

  updateTeam(team: Team): Observable<any> {
    return this.http.patch<any>('/api/teams/' + team.id, team).pipe(
      map(result => {
        const { id, created_at, updated_at, ...teamInfo } = result
        return teamInfo;
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    );
  }

  deleteTeam(removed: string): Observable<boolean> {
    return this.http.delete('api/teams/' + removed).pipe(
      map(result => {
        if (result) {
          //console.log("teamService deleted ", result)
          return true;
        } else {
          return false;
        }
      })
    );
  }

  getTeamAnalytics(): Observable<TeamAnalytics[]> {
    return this.http.get<any>('/api/teams/analytics.json').pipe(
      map(response => {
        //console.log('getTeamAnalytics', response);
        if (response.data) {
          //console.log(response.data)
          return response.data.map(item=> new TeamAnalytics(item));
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    );
  }

  ssfTeamAnalytics(params: string): Observable<TeamAnalytics[]> {
    return this.http.get<any>('/api/teams/analytics.json?' + params).pipe(
      map(response => {
        if (response.success && response.data && response.data.length >= 1)
        return response.data.map(item=> new TeamAnalytics(item));

        return null;
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    )
  }





}
