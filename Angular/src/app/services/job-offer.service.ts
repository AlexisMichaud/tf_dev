import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { JobOffer } from '../models/jobOffer.model';

@Injectable({
  providedIn: 'root'
})
export class JobOfferService {




  getJobOffer(jobId: string): Observable<JobOffer | null | boolean> {
    return this.http.get<any>('/api/job_offers/'+jobId + '.json').pipe(
      map(response => {
        //console.log('getjobs', response);
        if (response.data) {
          //console.log(response.data)
          const {created_at,updated_at, ...jobInfo} = response.data
          //console.log(jobInfo)
          //console.log(response)
          return jobInfo;
        } else {
          return false;
        }
      }),
      catchError(error => {
        console.log('Error', error);

        return of(false);
      })
    );
  }

  ssfJobOffer(params: string): Observable<JobOffer[]> {
    return this.http.get<any>('/api/job_offers.json?' + params).pipe(
      map(response => {
        if (response.success && response.data && response.data.length >= 1)
          return response.data;

        return null;
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    )
  }


  constructor(private http: HttpClient, private authService: AuthService) { }




}
