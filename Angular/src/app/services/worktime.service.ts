import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Worktime } from '../models/worktime.model';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WorktimeService {

  constructor(private http: HttpClient) { }

  ssfWorktimes(params: string, id: string): Observable<Worktime[]> {
    return this.http.get<any>('/api/task/' + id + '/worktimes.json?' + params).pipe(
      map(result => {
        if (result.success && result.data && result.data.length >= 1) {
          return result.data.map(item=> new Worktime(item));;
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    )
  }
  getWorktimes(task_id: string): Observable<Worktime[] | null> {
    return this.http.get<any>('/api/task/' + task_id + '/worktimes.json').pipe(
      map(result => {
        if (result.data) {
          return result.data.map(item=> new Worktime(item));
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(false);
      })
    );
  }
  getWorktime(worktime_id: string): Observable<any> {
    return this.http.get<any>('/api/worktimes/' + worktime_id + '.json').pipe(
      map(result => {
        if (result.data) {
          const { id,task_id, created_at, updated_at,duration, ...worktimeInfo } = result.data

          return worktimeInfo
        } else {
          console.log("currentWorktime", result)
          return null
        }
      })
    );
  }

  createWorktime(worktime: Worktime): Observable<any> {
    return this.http.post<any>('/api/worktimes', worktime).pipe(
      map(result => {
        if(result.success)
          return result.data;
        return false
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }
  updateWorktime(worktime_id:string,worktime: Worktime): Observable<any> {
    return this.http.patch<any>('/api/worktimes/' + worktime_id +'.json', worktime).pipe(
      map(result => {
        if(result.success){
        const { id, task_id, archived, created_at, updated_at, ...worktimeInfo } = result.data
          return worktimeInfo;
        }else{
        return false
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }

  deleteWorktime(removed: string): Observable<any> {
    return this.http.delete<any>('api/worktimes/' + removed).pipe(
      map(result => {
        if (result) {
          if(result.success)
            return result.data;
          return false
        } else {
          return false;
        }
      })
    );
  }
  archiveWorktime(worktime_id:string, worktime: Worktime): Observable<any> {
    return this.http.post<any>('/api/worktime/' + worktime_id +'/archive.json', worktime).pipe(
      map(result => {
        if(result.success)
          return result.data;
        return false
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }
}
