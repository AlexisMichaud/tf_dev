import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { EmployeeCredentials } from 'src/app/models/employee-credentials.model';
import Response from 'src/app/models/response.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _currentEmployee: Employee | null = null;

  get currentEmployee(): Employee | null {
    return this._currentEmployee;
  }

  constructor(private http: HttpClient) {}

  isLoggedIn(): Observable<boolean> {
    if (this.currentEmployee) {
      return of(true);
    }

    return this.http.get<any>('/isloggedin.json').pipe(
      map((response) => {
        if (response.success) {
          this._currentEmployee = new Employee(response.data.user);
          return true;
        } else {
          return false;
        }
      }),
      catchError((error) => {
        console.log('Error', error);

        return of(false);
      })
    );
  }

  logIn(employee: EmployeeCredentials): Observable<boolean> {
    return this.http
      .post<any>('/users/sign_in.json', { user: employee })
      .pipe(
        map((response) => {
          if (response.success) {
            this._currentEmployee = new Employee(response.data.user);
            return response.data;
          } else {
            return false;
          }
        }),
        catchError((error) => {
          console.log('Error', error);

          return of(false);
        })
      );
  }

  signUp(employee: EmployeeCredentials): Observable<any> {
    return this.http
      .post<any>('/users.json', { user: employee })
      .pipe(
        map((response) => {
          if (response.success) {
            this._currentEmployee = response.data.email;
            return response.data;
          } else {
            return false;
          }
        }),
        catchError((error) => {
          console.log('Error', error);

          return of(null);
        })
      );
  }

  update(employee: Employee): Observable<any> {
    return this.http
      .patch<any>('/users.json', { user: employee })
      .pipe(
        map((response) => {
          if (response.success) {
            console.log(response);
            this._currentEmployee = response.data.user;
            return response;
          } else {
            return false;
          }
        }),
        catchError((error) => {
          console.log('Error', error);

          return of(null);
        })
      );
  }

  showAvatar(): Observable<Response<{ avatar: string }>> {
    return this.http.get<Response<{ avatar: string }>>('/api/avatar').pipe(
      catchError((err) => {
        console.error(err);
        return of({ success: false as false, exception: err });
      })
    );
  }

  updateAvatar(b64: string): Observable<Response<{ avatar: string }>> {
    const data = { avatar: { data: b64 } };
    return this.http
      .post<Response<{ avatar: string }>>('/api/avatar', data)
      .pipe(
        catchError((err) => {
          console.error(err);
          return of({ success: false as false, exception: err });
        })
      );
  }

  logOut(): Observable<boolean> {
    return this.http.delete<any>('/users/sign_out').pipe(
      map((response) => {
        if (response.success) {
          this._currentEmployee = null;
          return response;
        } else {
          return false;
        }
      }),
      catchError((error) => {
        console.log('Error', error);

        return of(false);
      })
    );
  }
}
