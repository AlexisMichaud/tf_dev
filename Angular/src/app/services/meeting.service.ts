import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { Meeting } from "../models/meeting.model";
import Response from '../models/response.model';

@Injectable({
  providedIn: 'root'
})

export class MeetingService {
  constructor(private http: HttpClient) {
  }

  ssfMeetings(params: string): Observable<Response<Meeting[]>> {
    let url = `/api/meetings.json?${params}`;
    return this.http
      .get<Response<Meeting[]>>(url)
      .pipe(catchError(this.catcher));
  }

  create(meeting: Meeting): Observable<Response<Meeting>> {
    const url = '/api/meetings.json';
    const data = { meeting };
    return this.http
      .post<Response<Meeting>>(url, data)
      .pipe(catchError(this.catcher));
  }

  meetingDetail(id: number): Observable<Response<Meeting>> {
    const url = `/api/meetings/${id}.json`;
    return this.http
      .get<Response<Meeting>>(url)
      .pipe(catchError(this.catcher));
  }

  update(meeting: Meeting): Observable<Response<Meeting>> {
    const url = `/api/meetings/${meeting.id}.json`;
    const data = { meeting };
    return this.http
      .patch<Response<Meeting>>(url, data)
      .pipe(catchError(this.catcher));
  }

  archive(meeting: Meeting): Observable<Response<Meeting>> {
    const url = `/api/meeting/${meeting.id}/archive.json`;
    const data = { meeting };
    return this.http
      .post<Response<Meeting>>(url, data)
      .pipe(catchError(this.catcher));
  }

  delete(id: number): Observable<Response<Meeting>> {
    const url = `/api/meetings/${id}.json`;
    return this.http
      .delete<Response<Meeting>>(url)
      .pipe(catchError(this.catcher));
  }

  private catcher(error): Observable<Response<never>> {
    console.error(error);
    return of({ success: false, exception: error });
  }
}
