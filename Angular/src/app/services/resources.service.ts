import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Resource } from '../models/resource.model';
import { ResourceAnalytics } from '../models/resource-analytics.model';
import Response from '../models/response.model';

@Injectable({
  providedIn: 'root',
})
export class ResourcesService {
  constructor(private http: HttpClient) {}

  index(
    search?: string,
    status = 'available'
  ): Observable<Response<Resource[]>> {
    let url = `/api/resources.json?status=${status}`;
    if (search) {
      url += `&search=${search}`;
    }
    return this.http
      .get<Response<Resource[]>>(url)
      .pipe(catchError(this.catcher));
  }

  show(id: number): Observable<Response<Resource>> {
    const url = `/api/resources/${id}.json`;
    return this.http
      .get<Response<Resource>>(url)
      .pipe(catchError(this.catcher));
  }

  create(resource: Resource): Observable<Response<Resource>> {
    const url = '/api/resources.json';
    const data = { resource };
    return this.http
      .post<Response<Resource>>(url, data)
      .pipe(catchError(this.catcher));
  }

  update(
    id: number,
    resource: Partial<Resource>
  ): Observable<Response<Resource>> {
    const url = `/api/resources/${id}.json`;
    const data = { resource };
    return this.http
      .patch<Response<Resource>>(url, data)
      .pipe(catchError(this.catcher));
  }

  delete(id: number): Observable<Response<Resource>> {
    const url = `/api/resources/${id}.json`;
    return this.http
      .delete<Response<Resource>>(url)
      .pipe(catchError(this.catcher));
  }

  borrow(id: number): Observable<Response<Resource>> {
    const url = `/api/resources/${id}/borrow.json`;
    return this.http
      .post<Response<Resource>>(url, {})
      .pipe(catchError(this.catcher));
  }

  unborrow(id: number, state: number): Observable<Response<Resource>> {
    const url = `/api/resources/${id}/unborrow.json`;
    const data = { borrow: { state } };
    return this.http
      .post<Response<Resource>>(url, data)
      .pipe(catchError(this.catcher));
  }

  analytics(search?: string): Observable<Response<ResourceAnalytics[]>> {
    let url = '/api/resources/analytics.json';
    if (search) {
      url += `?search=${search}`;
    }
    return this.http
      .get<Response<ResourceAnalytics[]>>(url)
      .pipe(catchError(this.catcher));
  }

  private catcher(error): Observable<Response<never>> {
    console.error(error);
    return of({ success: false, exception: error });
  }
}
