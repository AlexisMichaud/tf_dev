import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { Project } from "../models/project.model";
import Response from '../models/response.model';
import { ProjectAnalytics } from "../models/project-analytics.model";

@Injectable({
  providedIn: 'root'
})

export class ProjectService {
  constructor(private http: HttpClient) { }

  ssfProjects(params: string): Observable<Response<Project[]>> {
    let url = `/api/projects.json?${params}`;
    return this.http
      .get<Response<Project[]>>(url)
      .pipe(catchError(this.catcher));
  }

  create(project: Project): Observable<Response<Project>> {
    const url = '/api/projects.json';
    const data = { project };
    return this.http
      .post<Response<Project>>(url, data)
      .pipe(catchError(this.catcher));
  }

  projectDetail(id: number): Observable<Response<Project>> {
    const url = `/api/projects/${id}.json`;
    return this.http
      .get<Response<Project>>(url)
      .pipe(catchError(this.catcher));
  }

  update(project: Project): Observable<Response<Project>> {
    const url = `/api/projects/${project.id}.json`;
    const data = { project };
    return this.http
      .patch<Response<Project>>(url, data)
      .pipe(catchError(this.catcher));
  }

  archive(project: Project): Observable<Response<Project>> {
    const url = `/api/project/${project.id}/archive.json`;
    const data = { project };
    return this.http
      .post<Response<Project>>(url, data)
      .pipe(catchError(this.catcher));
  }

  delete(id: number): Observable<Response<Project>> {
    const url = `/api/projects/${id}.json`;
    return this.http
      .delete<Response<Project>>(url)
      .pipe(catchError(this.catcher));
  }

  analytics(params: string): Observable<Response<ProjectAnalytics[]>> {
    let url = `/api/projects/analytics.json?${params}`;
    return this.http
      .get<Response<Project[]>>(url)
      .pipe(catchError(this.catcher));
  }

  private catcher(error): Observable<Response<never>> {
    console.error(error);
    return of({ success: false, exception: error });
  }
}
