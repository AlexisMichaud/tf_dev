import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Meeting } from '../models/meeting.model';
import Response from '../models/response.model';
import { Task } from '../models/task.model';
import { Team } from '../models/team.model';

export interface Home {
  team: Team;
  next_meeting?: Meeting;
  next_task?: Task;
  late_tasks: Task[];
}

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor(private http: HttpClient) {}

  index(): Observable<Response<Home>> {
    let url = `/api/home.json`;
    return this.http.get<Response<Home>>(url).pipe(catchError(this.catcher));
  }

  private catcher(error): Observable<Response<never>> {
    console.error(error);
    return of({ success: false, exception: error });
  }
}
