import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Employee } from '../models/employee.model';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Membership } from '../models/membership.model';

@Injectable({
  providedIn: 'root'
})
export class MembershipsService {

  constructor(private http: HttpClient) { }



  getAllEmployees(): Observable<Employee[] | null> {
    return this.http.get<any>('/api/memberships.json').pipe(
      map(response => {
        if (response.data) {
          //console.log('getAllEmployee', response);
          return response.data;
        } else {
          return false;
        }
      }),
      catchError(error => {
        console.log('Error', error);

        return of(false);
      })
    );
  }

  ssfAllEmployees(params: string): Observable<[]> {
    //console.log("params", params)
    return this.http.get<any>('/api/memberships.json?'+ params).pipe(
      map(response => {
        //console.log("response", response)
        if (response.success && response.data && response.data.length >= 1)

          return response.data;
        
        return null;
      }),
      catchError(error => {
        console.log('Error', error);

        return of(null);
      })
    )
  }




  AddMember(newMembers:number[],teamId: number ): Observable<any> {

    //console.log({newMembers,teamId})
    return this.http.post<any>('/api/memberships.json',{newMembers,teamId}).pipe(
      
       map(result => {
         if (result) {
           //console.log("add membership ", result)
           return true;
        } else {
          console.log("testMember",result)
           return false;
         }
       })
     );
   }



  deleteMember(removed: string): Observable<boolean> {
    return this.http.delete<any>('api/memberships/'+ removed+ '.json').pipe(
      map(result => {
        if (result) {
          //console.log("membershipsService deleted ", result)
          return true;
        } else {
          return false;
        }
      })
    );
  }





}
