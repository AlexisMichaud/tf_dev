import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Task } from '../models/task.model';
import { TaskAnalytics } from '../models/taskAnalytics';

@Injectable({
  providedIn: 'root'
})
export class TaskService {


  constructor(private http: HttpClient) { }

  getAllTasks(): Observable<Task[] | null> {
    return this.http.get<any>('/api/tasks.json').pipe(
      map(result => {
        if (result.data) {
          return result.data.map(item=> new Task(item));
        } else {
          return false;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(false);
      })
    );
  }
  getProjectTasks(project_id: string): Observable<Task[] | null> {
    return this.http.get<any>('/api/project/' + project_id + '/tasks.json').pipe(
      map(result => {
        if (result.data) {
          return result.data.map(item=> new Task(item));
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(false);
      })
    );
  }


  ssfTask(params: string): Observable<Task[]> {
    return this.http.get<any>('/api/tasks.json?' + params).pipe(
      map(result => {
        if (result.success && result.data && result.data.length >= 1) {
          return result.data.map(item=> new Task(item));
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    )
  }

  ssfProjectTasks(params: string, id: string): Observable<Task[]> {
    return this.http.get<any>('/api/project/' + id + '/tasks.json?' + params).pipe(
      map(result => {
        if (result.success && result.data && result.data.length >= 1) {
          return result.data.map(item=> new Task(item));
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    )
  }

  getTask(taskId: string): Observable<any> {
    return this.http.get<any>('/api/tasks/' + taskId + '.json').pipe(
      map(result => {
        if (result.data) {
          const { id, time_ratio, project_id, created_at, updated_at,worktimes_count, ...taskInfo } = result.data
          return taskInfo
        } else {
          console.log("currentTask", result)
          return null
        }
      })
    );
  }

  createTask(task: Task): Observable<any> {
    return this.http.post<any>('/api/tasks.json', task).pipe(
      map(result => {
        if(result.success)
          return result.data
        return false
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }

  updateTask(task_id:string, task: Task): Observable<any> {
    return this.http.patch<any>('/api/tasks/' + task_id + '.json', task).pipe(
      map(result => {
        if(result.success){
          const { id, time_ratio, project_id, created_at, updated_at,worktimes_count, ...taskInfo } = result.data
          return taskInfo
        }else{
          return false
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }

  deleteTask(removed: string): Observable<boolean> {
    return this.http.delete<any>('api/tasks/' + removed + '.json').pipe(
      map(result => {
        if (result) {
          if(result.success)
            return result.data
          return false
        } else {
          return false;
        }
      })
    );
  }

  archiveTask(task_id:string, task: Task): Observable<any> {
    return this.http.post<any>('/api/task/' + task_id +'/archive.json', task).pipe(
      map(result => {
        if(result.success)
          return result.data;
        return false
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }
  getAnalytics(): Observable<TaskAnalytics | null> {
    return this.http.get<any>('/api/tasks/analytics.json').pipe(
      map(result => {
        if (result.data) {
          let TaskAnalytic=new TaskAnalytics(result.data[0])
          const {id,... analyticsInfo} = TaskAnalytic
          return analyticsInfo;
        } else {
          return null;
        }
      }),
      catchError(error => {
        console.log('Error', error);
        return of(null);
      })
    );
  }
}
