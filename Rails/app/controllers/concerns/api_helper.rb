module ApiHelper
  extend ActiveSupport::Concern

  def render_success(data, **options)
    render json: { success: true, data: data }, **options
  end

  def render_error(error, **options)
    render json: { success: false, data: error }, **options
  end

  def render_unprocessable_entity(exception)
    render json: { success: false, data: exception.record.errors }, status: :unprocessable_entity
  end

  def render_not_found(exception)
    render json: { success: false, data: exception.message }, status: :not_found
  end
end
