class Api::ProjectsController < ApplicationController
  include ApiHelper
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  before_action :set_team
  before_action :set_project, only: [:show, :update, :destroy, :archive]

  def index
    @projects = @team.projects

    if @projects.count > 0
      if params[:status] != 'all'
        @projects = @projects.where(archived: params[:status] == 'archived' ? true : false)
      end
      
      if params[:search] != nil && params[:search] != 'null'
        @projects = @projects.where("MATCH (name, description) AGAINST (? IN BOOLEAN MODE)", params[:search])
      end
      
      render_success @projects
    end
  end
  
  def show
    render_success @project
  end

  def create
    @project = Project.new(project_params)
    @project.team_id = @team.id

    if @project.save
      render_success @project
    else
      render_error @project.errors, status: :unprocessable_entity
    end
  end

  def update
    if @project.update(project_params)
      render_success @project
    else
      render_error @project.errors, status: :unprocessable_entity
    end
  end

  def archive
    if @project.archive!
      render_success @project
    else
      render_error @project.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @project.destroy
      render_success @project
    else
      render_error @project.errors, status: :unprocessable_entity
    end
  end

  def analytics
    if @team
      team_id = current_user.user_team.id
      status = params[:status] == 'archived' ? true : false
      filter = params[:status] != 'all'
      search = params[:search] != nil && params[:search] != 'null'
      date_range = params[:date_range] != nil && params[:date_range] != 'null'

      @results = Project.get_stats(
        team_id, 
        params[:search], 
        status, 
        params[:date_range].split('to')[0], 
        params[:date_range].split('to')[1],
        filter,
        search,
        date_range
      )
      
      render_success @results
    else
      render_error @projects, status: :unauthorized
    end
  end

  private
    def project_params
      params.require(:project).permit(:due_date, :name, :description, :archived)
    end

    def set_project
      @project = @team.projects.find(params[:id])
    end

    def set_team
      if !current_user.user_team.nil?
        @team = current_user.user_team
      else
        render_error status: :unauthorized
      end
    end
end