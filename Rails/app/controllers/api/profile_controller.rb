class Api::ProfileController < ApplicationController
  include ApiHelper
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

  def show_avatar
    if current_user.avatar.representable?
      data = { avatar: url_for(current_user.avatar) }
      render_success data
    else
      render_error "no avatar"
    end
  end

  def update_avatar
    current_user.avatar.attach(avatar_params)
    data = { avatar: url_for(current_user.avatar) }
    render_success data
  end

  private
    def avatar_params
      params.require(:avatar).permit(:data)
    end
end
