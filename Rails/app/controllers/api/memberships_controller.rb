class Api::MembershipsController < ApplicationController
  include ApiHelper
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found


    def index
      @users = User.left_joins(:membership, :team).where(memberships: { id: nil },teams: { id: nil })


      if params[:search] != nil && params[:search] != 'null'
        @users = @users.where("MATCH (firstname, lastname, email) AGAINST (? IN BOOLEAN MODE)", params[:search])
      end

      if @users
        render_success @users
      else
        render_error @users.errors, status: :unprocessable_entity
      end

    end

    
    def create
      if current_user.user_team.owner_id == current_user.id
        before=Membership.all.length
        params[:newMembers].each do |a| 
        Membership.create(user_id: a , team_id: params[:teamId])
        end
        @memberships=Membership.all.length
      end

      if @memberships>before
       render_success @memberships, status: :created
      else
        render_error @memberships.errors, status: :unprocessable_entity
      end
    end

    def destroy
      if current_user.user_team.owner_id == current_user.id
        @memberships = Membership.find_by_user_id(params[:id])
      end
      if @memberships.destroy
        render_success @memberships
      else
        render_error @memberships.errors, status: :unprocessable_entity
      end
    end

    private
      def set_membership
        @memberships = Membership.find(params[:id])
      end
end
