class Api::JobOffersController < ApplicationController
    include ApiHelper
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
    before_action :set_job_offers, only: [:show, :update, :destroy]
    
    def index
        @job_offers = JobOffer.all

        if params[:search] != nil && params[:search] != 'null'
            @job_offers = @job_offers.where("MATCH (name, description) AGAINST (? IN BOOLEAN MODE)", params[:search])
        end
      
        if @job_offers
            render_success @job_offers
        else
            render_error @job_offers.errors, status: :unprocessable_entity
        end

    end
    
    def show
      if @job_offers
        render_success @job_offers
      else
        render_error @job_offers.errors, status: :unprocessable_entity
      end
    end
    
    private
      def set_job_offers
          @job_offers = JobOffer.find(params[:id])
      end
      def job_offers_params
          params.require(:job_offers).permit(:id,:name, :description, :availability, :nb_employee_looking_for)
      end
end