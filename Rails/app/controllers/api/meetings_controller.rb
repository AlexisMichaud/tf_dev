class Api::MeetingsController < ApplicationController
  #extend ActiveModel::Naming
  include ApiHelper
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  before_action :set_team
  before_action :set_meeting, only: [:show, :update, :destroy, :archive]
  before_action :is_archived, only: [:update, :destroy, :archive, :create]
  before_action :set_project_and_meetings, only: :index

  def index
    if @meetings.count > 0
      if params[:status] != 'all'
        @meetings = @meetings.where(archived: params[:status] == 'archived' ? true : false)
      end

      if params[:search] != nil && params[:search] != 'null'
        @meetings = @meetings.where("MATCH (name, summary, location) AGAINST (? IN BOOLEAN MODE)", params[:search])
      end
      
      render_success @meetings
    end
  end
  
  def show
    render_success @meeting
  end  

  def create
    @meeting = Meeting.new(meeting_params)
    @meeting.project_id = @team.projects.find(params[:meeting][:project_id]).id

    if @meeting.save
      render_success @meeting
    else
      render_error @meeting.errors, status: :unprocessable_entity
    end
  end

  def update
    if @meeting.update(meeting_params)
      render_success @meeting
    else
      render_error @meeting.errors, status: :unprocessable_entity
    end
  end

  def archive
    if @meeting.archive!
      render_success @meeting
    else
      render_error @meeting.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @meeting.destroy
      render_success @meeting
    else
      render_error @meeting.errors, status: :unprocessable_entity
    end
  end

  private
    def meeting_params
      params.require(:meeting).permit(:name, :summary, :location, :start_dt, :end_dt, :archived)
    end

    def set_meeting
      @meeting = @team.meetings.find(params[:id])
    end

    def set_project_and_meetings
      @project = @team.projects.find(params[:project])
      @meetings = @project.meetings
    end

    def set_team
      if !current_user.user_team.nil?
        @team = current_user.user_team
      else
        render_error status: :unauthorized
      end
    end

    def is_archived
      if @team.projects.find(@meeting ? @meeting.project_id : params[:meeting][:project_id]).archived?
        render_error status: ":unprocessable_entity"
      end
    end
end
