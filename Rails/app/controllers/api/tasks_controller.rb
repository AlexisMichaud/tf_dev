class Api::TasksController < ApplicationController
  include ApiHelper
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  before_action :set_task, only: [:show, :update, :destroy, :archive]
  before_action :find_team
  before_action :is_allowed, only: [:show, :update, :destroy]

  def index
    tasks = current_user.user_team.tasks
    if(params[:state]!="all")
      if (params[:state])
        tasks=tasks.where(state: params[:state])
      end
    end
    if params[:search] != nil && params[:search] != 'null'
        tasks = tasks.where("MATCH (tasks.name, tasks.description) AGAINST (? IN BOOLEAN MODE)", params[:search])
    end
    
    if tasks
      render_success tasks
    else
      render_error tasks.errors, status: :unprocessable_entity
    end
  end

  def project_tasks
    project=current_user.user_team.projects.find(params[:id])
    tasks=project.tasks
    if(params[:state]!="all")
      if (params[:state])
        tasks=tasks.where(state: params[:state])
      end
    end
    if params[:search] != nil && params[:search] != 'null'
      tasks = tasks.where("MATCH (name, description) AGAINST (? IN BOOLEAN MODE)", params[:search])
    end
 
    if tasks
      render_success tasks
    else
      render_error tasks.errors, status: :unprocessable_entity
    end
  end

  def show
    if @task
      render_success @task
    else
      render_error @task.errors, status: :unprocessable_entity
    end
  end

  def create
    @task = Task.new(task_params)
    if current_user.team.project_ids.include?(@task.project_id)
      if @task.save
        render_success @task, status: :created
      else
        render_error @task.errors, status: :unprocessable_entity
      end
    else
      render_error status: :unauthorized
    end
  end

  def update
    if @task.update(task_params)
      render_success @task
    else
      render_error @task.errors, status: :unprocessable_entity
    end
  end

  def archive
    if @task.archive!
      render_success @task
    else
      render_error @task.errors, status: :unprocessable_entity
    end 
  end

  def destroy
    if @task.destroy
      render_success @task
    else
      render_error @task.errors, status: :unprocessable_entity
    end
  end

  def analytics
    projectIds=current_user.team.project_ids
    userIds=current_user.team.member_ids
    @analytics=Task.find_by_sql(["SELECT 
    (SELECT COUNT(*)*100/(SELECT COUNT(*) FROM tasks WHERE project_id IN (:projectIds)) FROM tasks WHERE project_id IN (:projectIds) AND state=0) as activePercent,
    (SELECT COUNT(*)*100/(SELECT COUNT(*) FROM tasks WHERE project_id IN (:projectIds)) FROM tasks WHERE project_id IN (:projectIds) AND actual_time>estimated_time)as percentOverTime,
    (SELECT AVG(actual_time) FROM tasks WHERE project_id IN (:projectIds)) as avgTime,
    (SELECT COUNT(1) FROM worktimes WHERE user_id IN(:userIds)) as worktimeCount,
    (SELECT maxTime.duration FROM (SELECT SUM(duration) as duration,user_id,user as name FROM worktimes WHERE user_id IN (:userIds) GROUP BY user_id ORDER BY duration DESC LIMIT 1)as maxTime) as mostTime,
    (SELECT maxTime.name FROM (SELECT SUM(duration) as duration,user_id,user as name FROM worktimes WHERE user_id IN (:userIds) GROUP BY user_id ORDER BY duration DESC LIMIT 1)as maxTime) as mostTimeName,
    (SELECT COUNT(*)*100/(SELECT COUNT(*) FROM worktimes WHERE user_id IN (:userIds)) FROM worktimes WHERE user_id IN (:userIds) AND problem IS NOT NULL) as percentProblem
    ",projectIds: projectIds,userIds: userIds])

    if @analytics
      render_success @analytics.as_json
    else
      render_error @task.errors, status: :unprocessable_entity
    end
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:name, :description, :state, :estimated_time, :actual_time, :due_date, :archived,:project_id)
    end


    def is_allowed
      if !@task.project.team.member_ids.include?(current_user.id)
        render_error status: :unauthorized
      end
    end

    def find_team
      if current_user.user_team.nil?
        render_error status: :unauthorized
      end
    end

end
