class Api::WorktimesController < ApplicationController
    include ApiHelper
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
    before_action :set_worktime, only: [:show, :update, :destroy, :archive]
    before_action :is_allowed, only: [:show, :update, :destroy]
    before_action :find_team
    before_action :can_edit, only: [:update,:delete]
    
    def index
        worktimes = Worktime.where(task_id: params[:id])
        if (params[:archived])
          worktimes=worktimes.where(archived: params[:archived])
        end
        if params[:search] != nil && params[:search] != 'null'
            worktimes = worktimes.where("MATCH (name, description) AGAINST (? IN BOOLEAN MODE)",params[:search])
        end
        
        if worktimes
          render_success worktimes
        else
          render_error worktimes.errors, status: :unprocessable_entity
        end
    end

    def show
      if @worktime
        render_success @worktime
      else
        render_error @worktime.errors, status: :unprocessable_entity
      end
    end
    
    def create
      @worktime = Worktime.new(worktime_params)
      @worktime.user_id=current_user.id
      @worktime.user=current_user.fullname
      if current_user.team.task_ids.include?(@worktime.task_id)
        if @worktime.save
          render_success @worktime,status: :created
        else
          render_error @worktime.errors, status: :unprocessable_entity
        end
      else
        render_error status: :unauthorized
      end
    end
  
    def update
      if @worktime.update(worktime_params)
        render_success @worktime
      else
        render_error @worktime.errors, status: :unprocessable_entity
      end
    end
  
    def archive
      if @worktime.archive!
        render_success @worktime
      else
        render_error @worktime.errors, status: :unprocessable_entity
      end 
    end
  
    def destroy
      if @worktime.destroy
        render_success @worktime
      else
        render_error @worktime.errors, status: :unprocessable_entity
      end
    end

    private
    def set_worktime
      @worktime = Worktime.find(params[:id])
    end

    def worktime_params
      params.require(:worktime).permit(:name, :description,:problem,:start_dt,:end_dt, :archived,:task_id)
    end

    def is_allowed
      if !@worktime.task.project.team.member_ids.include?(current_user.id)
        render_error status: :unauthorized
      end
    end
    def can_edit
      if !@worktime.user_id==current_user.id
        render_error status: :unauthorized
      end
    end
    
    def find_team
      if current_user.user_team.nil?
        render_error status: :unauthorized
      end
    end
end
