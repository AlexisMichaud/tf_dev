class Api::HomeController < ApplicationController
  include ApiHelper
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

  def index
    team = current_user.user_team
    if team == nil
      render_error "Current user is not in a team"
      return
    end

    next_meeting = Meeting.order(start_dt: :asc).where("start_dt > ?", DateTime.current)
                          .includes(:project).where(project: { team_id: team.id })
                          .first
    next_task = Task.order(due_date: :asc).where(state: 0)
                    .includes(:project).where(project: { team_id: team.id })
                    .first

    late_tasks = Task.order(due_date: :asc).where(state: 0).where("due_date <= ?", DateTime.current)

    data = { team: team, next_meeting: next_meeting, next_task: next_task, late_tasks: late_tasks }
    render_success data
  end
end
