class Api::ResourcesController < ApplicationController
  include ApiHelper

  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
  before_action :set_resource, only: [:show, :update, :destroy, :borrow, :unborrow]

  def index
    @resources = Resource.all

    if params[:search]
      @resources = @resources.where("MATCH (model, brand, description) AGAINST (? IN BOOLEAN MODE)", params[:search])
    end

    if params[:status] == "mine"
      @resources = @resources.left_joins(:borrows).where(borrows: { user_id: current_user.id, end_date: nil })
    elsif params[:status] == "available"
      @resources = @resources.available
    end

    render_success @resources
  end

  def show
    render_success @resource.as_json
  end

  def create
    @resource = Resource.new(resource_params)
    if @resource.save
      render_success @resource, status: :created
    else
      render_error @resource.errors, status: :unprocessable_entity
    end
  end

  def update
    if @resource.update(resource_params)
      render_success @resource
    else
      render_error @resource.errors, status: :unprocessable_entity
    end
  end

  def destroy
    json = @resource.as_json
    @resource.destroy
    render_success json
  end

  def borrow
    unless @resource.is_available?
      render_error "This resource is not available", status: :unprocessable_entity
      return
    end

    borrow = Borrow.new(user: current_user, resource: @resource, start_date: Date.current)
    if borrow.save
      render_success @resource.as_json, status: :created
    else
      render_error borrow.errors, status: :unprocessable_entity
    end
  end

  def unborrow
    current_borrow = @resource.current_borrow
    unless current_borrow and current_borrow.user_id == current_user.id
      render_error "You are not the current owner of this resource", status: :unprocessable_entity
      return
    end

    if current_borrow.update(end_date: Date.current, state: borrow_params["state"])
      render_success @resource.as_json
    else
      render_error current_borrow.errors, status: :unprocessable_entity
    end
  end

  def analytics
    if params[:search]
      analytics = Resource.find_by_sql(["
      SELECT r.id, model, brand, description, COUNT(b.id) AS borrow_count, COALESCE(SUM(DATEDIFF(COALESCE(end_date, CURRENT_DATE), start_date)), 0) AS days_borrowed, (
        SELECT CONCAT(firstname, ' ', lastname)
        FROM users u
        WHERE u.id = b.user_id
        GROUP BY u.id
        ORDER BY COUNT(*) DESC
        LIMIT 1
      ) AS top_borrower
      FROM resources r
      LEFT JOIN borrows b
      ON r.id = b.resource_id
      WHERE MATCH (model, brand, description) AGAINST (? IN BOOLEAN MODE)
      GROUP BY r.id
      ", params[:search]])
      render_success analytics
    else
      analytics = Resource.find_by_sql("
      SELECT r.id, model, brand, description, COUNT(b.id) AS borrow_count, COALESCE(SUM(DATEDIFF(COALESCE(end_date, CURRENT_DATE), start_date)), 0) AS days_borrowed, (
        SELECT CONCAT(firstname, ' ', lastname)
        FROM users u
        WHERE u.id = b.user_id
        GROUP BY u.id
        ORDER BY COUNT(*) DESC
        LIMIT 1
      ) AS top_borrower
      FROM resources r
      LEFT JOIN borrows b
      ON r.id = b.resource_id
      GROUP BY r.id
      ")
      render_success analytics
    end
  end

  private
    def set_resource
      @resource = Resource.find(params[:id])
    end
    def resource_params
      params.require(:resource).permit(:brand, :model, :description)
    end
    def borrow_params
      params.require(:borrow).permit(:state)
    end
end
