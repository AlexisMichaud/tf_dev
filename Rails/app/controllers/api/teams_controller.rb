class Api::TeamsController < ApplicationController
  include ApiHelper
  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found


  def index
    @team = current_user.user_team
    if @team
      render_success @team
    end
  end


  def create
    @team = Team.new(team_params)
    @team.owner_id = current_user.id
    if @team.save
      render_success @team, status: :created
    else
      render_error @team.errors, status: :unprocessable_entity
    end
  end

  def findMember
    @members = current_user.user_team.team_members
    if(params[:availability]!="all")
      if (params[:availability])
        @members=@members.where(availability: params[:availability])
      end
    end

    if params[:search] != nil && params[:search] != 'null'
      @members = @members.where("MATCH (firstname, lastname,email) AGAINST (? IN BOOLEAN MODE)", params[:search])
    end

    if @members
      render_success @members
    else
      render_error @members.errors, status: :unprocessable_entity
    end

  end


  def update
    if current_user.user_team.owner_id == current_user.id
      @team = Team.find(params[:id])
    end 
    if @team.update(team_params)
      render_success @team
    else
      render_error @team.errors, status: :unprocessable_entity
    end
  end

  

  def destroy
    if current_user.user_team.owner_id == current_user.id
      @team = Team.find(params[:id])
    end
    Membership.where(team_id: @team.id).destroy_all
    if @team.destroy
      render_success @team
    else
      render_error @team.errors, status: :unprocessable_entity
    end
  end

  def analytics
    if params[:search] == nil || params[:search] == 'null'
    @analytics=Team.find_by_sql(["
    with teamMembers AS (select u.lastname,u.firstname, u.availability, t.id as team_id from users u
    join teams t on t.owner_id = u.id
    union
    select u.lastname,u.firstname, u.availability, m.team_id from users u
    join memberships m on m.user_id = u.id)
    select t.id as team_id,t.name,u.lastname,u.firstname,
    (select count(1) from teamMembers tm where t.id = tm.team_id and availability =0)AS nbOfFullTime,
    (select count(1) from teamMembers tm where t.id = tm.team_id and availability =1)AS nbOfHalfTime,
    (select count(1) from teamMembers tm where t.id = tm.team_id and availability =2)AS nbOfContractual,
    (select count(1) from teamMembers tm where t.id = tm.team_id) AS numberOfMember,
    (select DATEDIFF(CURDATE(),(SELECT t.created_at FROM teams t where t.id = team_id))) AS age,
    (select COALESCE(sum(j.nb_employee_looking_for), 0) from job_offers j where j.team_id = t.id) AS numberOfPost
    from teams t join users u on u.id = t.owner_id"])

    end

    if params[:search] != nil && params[:search] != 'null'
    @analytics=Team.find_by_sql(["
    with teamMembers AS (select u.lastname, u.availability, t.id as team_id from users u
    join teams t on t.owner_id = u.id
    union
    select u.lastname, u.availability, m.team_id from users u
    join memberships m on m.user_id = u.id )
    select t.id as team_id,t.name,u.lastname,u.firstname,
    (select count(1) from teamMembers tm where t.id = tm.team_id and availability =0)AS nbOfFullTime,
    (select count(1) from teamMembers tm where t.id = tm.team_id and availability =1)AS nbOfHalfTime,
    (select count(1) from teamMembers tm where t.id = tm.team_id and availability =2)AS nbOfContractual,
    (select count(1) from teamMembers tm where t.id = tm.team_id) AS numberOfMember,
    (select DATEDIFF(CURDATE(),(SELECT t.created_at FROM teams t where t.id = team_id))) AS age,
    (select COALESCE(sum(j.nb_employee_looking_for), 0) from job_offers j where j.team_id = t.id) AS numberOfPost
    from teams t join users u on u.id = t.owner_id and MATCH(lastname,firstname,email) AGAINST ((:search) IN BOOLEAN MODE)" , search: params[:search]])
    end

    if @analytics
      render_success  @analytics
    else
      render_error @analytics.errors, status: :unprocessable_entity
    end
  end



  private

    def team_params
      params.require(:team).permit(:id,:name, :description, :message, :message_expiration)
    end

end
