# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def isLoggedIn
    if user_signed_in?
      render json: { success: true, data: { user: current_user } }
    else
      render json: { success: false, data: { errors: "Null current user." } }
    end
  end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)
    render json: { success: true, data: { user: current_user } }
  end

  # DELETE /resource/sign_out
  def destroy
    sign_out()
    render json: { success: true }
  end

  # protected


  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
