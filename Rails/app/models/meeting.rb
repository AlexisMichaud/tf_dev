class Meeting < ApplicationRecord
    validate :start_dt_cannot_be_in_the_past
    validate :end_dt_bigger_than_start_dt
    validates :name, length: { maximum: 45 }, presence: true
    validates :summary, length: { maximum: 255 }, presence: true
    validates :location, length: { maximum: 45 }, presence: true

    belongs_to :project

    def archive!
        self.update_attribute('archived', !self.archived)
    end

    private 
    def start_dt_cannot_be_in_the_past
        if start_dt < Date.today
        errors.add(:start_dt, "Can't be in the past")
        end
    end

    def end_dt_bigger_than_start_dt
        if end_dt < start_dt
        errors.add(:end_dt, "Can't be lesser than start time")
        end
    end
end
