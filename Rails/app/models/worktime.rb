class Worktime < ApplicationRecord
    validate  :end_must_be_after_start
    validates :name, length: { maximum: 45 }, presence: true
    validates :description, length: { maximum: 255 }, presence: true
    validates :user, presence:true
    validates :duration, numericality: { greater_than_or_equal: 0,  only_integer: true }
    before_save :check_duration
    after_save :check_total_time
    after_destroy :check_total_time
    belongs_to :task
    
    def check_duration
        if end_dt
            self.duration = ((end_dt - start_dt)/60).to_i
        else 
            self.duration = 0
        end
    end

    def check_total_time
        self.task.change_time
    end

    def archive!
        if !self.archived
            self.update_attribute('archived', true)
        else
            self.update_attribute('archived', false)
        end
    end

    def end_must_be_after_start
        if end_dt
            if end_dt <= start_dt
                errors.add(:end_time, "must be after start time")
            end
        end
    end
end