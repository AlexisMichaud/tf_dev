class Team < ApplicationRecord
  validates :name, length: { maximum: 45 }, presence: true
  validates :description, length: { maximum: 255 }, presence: true

  validates :message, allow_nil: true , presence: true, if: :message_chek
  validates :message_expiration, presence: true, if: :message_expiration_chek

  belongs_to :owner, class_name: "User", foreign_key: "owner_id"
  has_many :memberships
  has_many :job_offers, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_many :tasks, through: :projects
  has_many :meetings, through: :projects

  has_many :users, through: :memberships

  def team_members
    return self.users
  end

  def team_members
    return self.users
  end

  def members
    return (self.users + [self.owner])
  end


  def member_ids
    return (self.user_ids + [self.owner_id])
  end

  private def message_expiration_chek
    if message != "" && message != nil
      if message_expiration == nil 
        errors.add(:message_expiration, "can't be null if message is not null")
      end
    end
  end
  private def message_chek
    if message_expiration != nil
      if message == ""
        errors.add(:message, "can't be null if message expiration is not null")
      end
    end
  end



end
