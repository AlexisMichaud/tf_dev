class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
         
  validates :firstname, length: { maximum: 45 }, presence: true
  validates :lastname, length: { maximum: 45 }, presence: true

  validates :availability, inclusion: { in: [0, 1, 2] }

  validates :phone, length: { is: 10 }, numericality: { only_integer: true }, allow_nil: true
  validates :address, length: { in: 1..255 }, allow_nil: true


  has_one :membership
  has_one :team, foreign_key: "owner_id"
  has_many :borrows

  has_one_base64_attached :avatar

  def user_team
    if self.membership
      return self.membership.team
    else
      return self.team
    end
  end

  def fullname
    "#{firstname} #{lastname}"
  end
end
