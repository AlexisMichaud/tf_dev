class Project < ApplicationRecord
  validate :due_date_cannot_be_in_the_past
  validates :name, length: { maximum: 45 }, presence: true
  validates :description, length: { maximum: 255 }, presence: true

  belongs_to :team
  has_many :tasks, dependent: :destroy
  has_many :meetings, dependent: :destroy

  def archive!
    self.tasks.each do | t |
      t.archive!
    end
    self.meetings.each do | m |
      if m.archived? == self.archived?
        m.archive!
      end
    end
    self.update_attribute('archived', !self.archived)
  end

  def self.get_stats(team_id, search, status, from, to, do_filter, do_search, do_date_range)
    @stats = find_by_sql(["SELECT
      (SELECT COUNT(m_count) FROM 
        " + getSpecificFilteredList("tot_meetings", do_filter, do_search, do_date_range) + " AS m_tot)
      AS tot_meetings,

      ROUND (
        (SELECT COUNT(p_id) * 100 FROM 
        " + getSpecificFilteredList("active_percentage", do_filter, do_search, do_date_range) + " AS p_percent_numerator) 
          / 
        (SELECT COUNT(p_id) FROM 
        " + getSpecificFilteredList("projects_count", do_filter, do_search, do_date_range) + " AS p_percent_denumerator)
      ) AS active_percentage,

      ROUND (
        (SELECT duration_sum FROM 
        " + getSpecificFilteredList("tot_duration_meetings", do_filter, do_search, do_date_range) + " AS m_time_numerator) 
          / 
        (SELECT COUNT(m_count) FROM 
        " + getSpecificFilteredList("tot_meetings", do_filter, do_search, do_date_range) + " AS m_time_denumerator) 
      , 2) AS avg_meeting_time,

      ROUND (
        (SELECT COUNT(m_count) FROM 
        " + getSpecificFilteredList("tot_meetings", do_filter, do_search, do_date_range) + " AS m_count_numerator) 
          / 
        (SELECT COUNT(p_id) FROM 
        " + getSpecificFilteredList("projects_count", do_filter, do_search, do_date_range) + " AS m_count_denumerator)
      , 2 ) AS avg_meeting_count,

      (SELECT m_count FROM 
        " + getSpecificFilteredList("most_meetings_count", do_filter, do_search, do_date_range) + " AS p_most_meetings)
      AS most_meetings_count,

      (SELECT p_name FROM 
        " + getSpecificFilteredList("most_meetings_project", do_filter, do_search, do_date_range) + " AS p_most_meetings)
      AS most_meetings_project
      ", 
      {
        :t_id => team_id,
        :p_search => search, 
        :p_status => status, 
        :from => from, 
        :to => to 
      }
    ]).as_json

    @projects = find_by_sql(["
      SELECT p.id, p.name, p.archived, p.description, (SELECT COUNT(*) FROM meetings m WHERE m.project_id = p.id) AS meetings_count
      FROM projects p, teams t, meetings m 
      WHERE t.id = :t_id AND p.team_id = :t_id " +
      ( do_filter ? "AND p.archived = :p_status " : "" ) +
      ( do_search ? "AND MATCH (p.name, p.description) AGAINST (:p_search IN BOOLEAN MODE) " : "" ) +
      ( do_date_range ? "AND p.due_date BETWEEN :from AND :to " : "" ) +
      "GROUP BY p.id",
      {
        :t_id => team_id, 
        :p_search => search, 
        :p_status => status, 
        :from => from, 
        :to => to 
      }
    ]).as_json

    return @stats + @projects
  end

  private 
  def self.getSpecificFilteredList(intent, do_filter, do_search, do_date_range)
    return "(
      SELECT SUM(HOUR(TIMEDIFF(m.end_dt, m.start_dt))) AS duration_sum, COUNT(m.id) AS m_count, p.name AS p_name, p.id AS p_id " +

      "FROM projects p, teams t, meetings m WHERE t.id = :t_id AND p.team_id = :t_id " +

      ( intent == "tot_duration_meetings" || 
        intent == "tot_meetings" || 
        intent == "most_meetings_count" || 
        intent == "most_meetings_project" ? 
      "AND m.project_id = p.id " : "" ) +
      ( intent == "active_percentage" ? "AND p.archived = false " : "" ) +

      ( do_filter ? "AND p.archived = :p_status " : "" ) +
      ( do_search ? "AND MATCH (p.name, p.description) AGAINST (:p_search IN BOOLEAN MODE) " : "" ) +
      ( do_date_range ? "AND p.due_date BETWEEN :from AND :to " : "" ) + 

      ( intent == "tot_duration_meetings" ? ") " : "" ) + 
      ( intent == "tot_meetings" ? "GROUP BY m.id) " : "" ) + 
      ( intent == "active_percentage" || intent == "projects_count" ? "GROUP BY p.id) " : "" ) +
      ( intent == "most_meetings_project" || intent == "most_meetings_count" ? "GROUP BY p.id ORDER BY COUNT(m.id) DESC LIMIT 1) " : "" )
  end

  def due_date_cannot_be_in_the_past
    if due_date < Date.today
      errors.add(:due_date, "Can't be in the past")
    end
  end
end