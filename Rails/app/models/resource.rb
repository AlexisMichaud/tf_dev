class Resource < ApplicationRecord
  validates :brand, length: { maximum: 45 }, presence: true
  validates :model, length: { maximum: 45 }, presence: true
  validates :description, length: { maximum: 255 }, presence: true

  has_many :borrows, -> { order(start_date: :desc) }, dependent: :destroy

  scope :available, -> {
    left_joins(:borrows).where.not(borrows: Borrow.current).distinct
  }

  def current_borrow
    borrows.current.first
  end
  def is_available?
    current_borrow == nil
  end

  def state_id
    last_borrow = borrows.past.first
    if last_borrow != nil
      last_borrow.state
    else
      0
    end
  end
  include ResourceState

  def as_json(options = { include: { borrows: { methods: [:state_id, :state_name, :user_fullname], exclude: :state } } })
    super(methods: [:state_id, :state_name], **options)
  end
end
