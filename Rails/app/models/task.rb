class Task < ApplicationRecord
  validate  :due_date_cannot_be_in_the_past
  validates :name, length: { maximum: 45 }, presence: true
  validates :description, length: { maximum: 255 }, presence: true
  validates :state, inclusion: { in: [0, 1, 2] }
  validates :estimated_time,numericality: { greater_than: 15,  only_integer: true }
  belongs_to :project
  has_many :worktimes, dependent: :destroy


  def change_time
    self.actual_time=0
    self.worktimes.each do |w|
      self.actual_time +=w.duration
    end
    self.save
  end
  
  def archive!
    if self.state==2
        self.worktimes.each do |w|
          w.archive!
        end
      self.update_attribute('state', 1)
    else
        self.worktimes.each do |w|
          w.archive!
        end
      self.update_attribute('state', 2)
    end
  end

  private def due_date_cannot_be_in_the_past
    if due_date < Date.today
      errors.add(:due_date, "can't be in the past")
    end
  end
end
