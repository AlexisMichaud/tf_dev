class JobOffer < ApplicationRecord
    validates :name, length: { maximum: 45 }, presence: true
    validates :description, length: { maximum: 255 }, presence: true
    validates :availability, inclusion: { in: [0, 1, 2] }
    validates :nb_employee_looking_for, presence: true

    belongs_to :team
  
end
