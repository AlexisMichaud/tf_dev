module ResourceState
  extend ActiveSupport::Concern

  def state_name
    case state_id
    when 0
      "New"
    when 1
      "Used"
    when 2
      "Damaged"
    else
      nil
    end
  end
end