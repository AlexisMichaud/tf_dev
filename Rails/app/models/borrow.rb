class Borrow < ApplicationRecord
  validates :state, inclusion: { in: [nil, 0, 1, 2] }

  belongs_to :resource
  belongs_to :user

  scope :current, -> { where(end_date: nil) }
  scope :past, -> { where.not(end_date: nil) }

  def user_fullname
    user.fullname
  end
  def state_id
    state
  end
  include ResourceState

  def as_json(options = {})
    super(methods: [:state_id, :state_name, :user_fullname], exclude: :state, **options)
  end
end
