# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_04_230015) do

  create_table "active_storage_attachments", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", charset: "utf8mb4", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", charset: "utf8mb4", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "borrows", charset: "utf8mb4", force: :cascade do |t|
    t.date "start_date", null: false
    t.date "end_date"
    t.integer "state"
    t.bigint "resource_id", null: false
    t.bigint "user_id", null: false
    t.index ["resource_id"], name: "index_borrows_on_resource_id"
    t.index ["user_id"], name: "index_borrows_on_user_id"
  end

  create_table "job_offers", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "description", null: false
    t.integer "availability", default: 0, null: false
    t.integer "nb_employee_looking_for", default: 1, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "team_id", null: false
    t.index ["name", "description"], name: "job_offers_search", type: :fulltext
    t.index ["team_id"], name: "index_job_offers_on_team_id"
  end

  create_table "meetings", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "summary", limit: 45, null: false
    t.string "location", limit: 45, null: false
    t.datetime "start_dt", null: false
    t.datetime "end_dt", null: false
    t.boolean "archived", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "project_id", null: false
    t.index ["name", "summary", "location"], name: "meetings_search", type: :fulltext
    t.index ["project_id"], name: "index_meetings_on_project_id"
  end

  create_table "memberships", charset: "utf8mb4", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.bigint "team_id", null: false
    t.index ["team_id"], name: "index_memberships_on_team_id"
    t.index ["user_id"], name: "index_memberships_on_user_id", unique: true
  end

  create_table "projects", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "description", null: false
    t.datetime "due_date", null: false
    t.boolean "archived", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "team_id", null: false
    t.index ["name", "description"], name: "projects_search", type: :fulltext
    t.index ["team_id"], name: "index_projects_on_team_id"
  end

  create_table "resources", charset: "utf8mb4", force: :cascade do |t|
    t.string "brand", limit: 45, null: false
    t.string "model", limit: 45, null: false
    t.string "description", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["brand", "model", "description"], name: "resources_search", type: :fulltext
  end

  create_table "tasks", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "description", null: false
    t.integer "state", default: 0, null: false
    t.integer "estimated_time", null: false
    t.integer "actual_time", default: 0
    t.datetime "due_date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "project_id", null: false
    t.index ["name", "description"], name: "tasks_search", type: :fulltext
    t.index ["project_id"], name: "index_tasks_on_project_id"
  end

  create_table "teams", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "description", null: false
    t.string "message"
    t.datetime "message_expiration"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "owner_id", null: false
    t.index ["name", "description"], name: "teams_search", type: :fulltext
    t.index ["owner_id"], name: "index_teams_on_owner_id"
  end

  create_table "users", charset: "utf8mb4", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "firstname", limit: 45, null: false
    t.string "lastname", limit: 45, null: false
    t.string "phone", limit: 10
    t.string "address"
    t.integer "availability", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email", "firstname", "lastname"], name: "users_search", type: :fulltext
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "worktimes", charset: "utf8mb4", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.string "description", null: false
    t.string "problem"
    t.string "user", null: false
    t.integer "duration", default: 0
    t.datetime "start_dt", default: -> { "current_timestamp()" }, null: false
    t.datetime "end_dt"
    t.boolean "archived", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.bigint "task_id", null: false
    t.index ["name", "description"], name: "worktimes_search", type: :fulltext
    t.index ["task_id"], name: "index_worktimes_on_task_id"
    t.index ["user_id"], name: "index_worktimes_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "borrows", "resources"
  add_foreign_key "borrows", "users"
  add_foreign_key "job_offers", "teams"
  add_foreign_key "meetings", "projects"
  add_foreign_key "memberships", "teams"
  add_foreign_key "memberships", "users"
  add_foreign_key "projects", "teams"
  add_foreign_key "tasks", "projects"
  add_foreign_key "teams", "users", column: "owner_id"
  add_foreign_key "worktimes", "tasks"
  add_foreign_key "worktimes", "users"
end
