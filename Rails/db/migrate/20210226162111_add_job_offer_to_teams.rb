class AddJobOfferToTeams < ActiveRecord::Migration[6.1]
  def change
    add_reference :job_offers, :team, null: false, foreign_key: true
  end
end
