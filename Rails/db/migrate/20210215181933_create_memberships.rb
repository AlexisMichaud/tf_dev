class CreateMemberships < ActiveRecord::Migration[6.1]
  def change
    create_table :memberships do |t|
      t.timestamps
    end

    add_reference :memberships, :user, null: false,index:{unique: true}, foreign_key: true
    add_reference :memberships, :team, null: false, foreign_key: true
  end
end
