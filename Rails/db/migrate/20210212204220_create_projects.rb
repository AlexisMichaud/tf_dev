class CreateProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :projects do |t|
      t.string :name, null: false, limit: 45
      t.string :description, null: false, limit: 255
      t.datetime :due_date, null: false
      t.boolean :archived, null: false, default: false

      t.timestamps
    end

    add_index :projects, [:name, :description], name: 'projects_search', type: :fulltext
  end
end
