class CreateBorrows < ActiveRecord::Migration[6.1]
  def change
    create_table :borrows do |t|
      t.date :start_date, null: false, default: -> { "CURRENT_DATE" }
      t.date :end_date, default: -> { 'NULL' }
      # 0: New, 1: Used, 2: Damaged
      t.integer :state, default: -> { 'NULL' }
    end
    add_reference :borrows, :resource, null: false, foreign_key: true
    add_reference :borrows, :user, null: false, foreign_key: true
  end
end
