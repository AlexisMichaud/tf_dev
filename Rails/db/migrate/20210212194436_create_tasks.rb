class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :name, null: false, limit: 45
      t.string :description, null: false, limit: 255
      # 0: InProgress, 1: Finished
      t.integer :state, null: false, default: 0
      t.integer :estimated_time, null: false
      t.integer :actual_time,default: 0
      t.datetime :due_date, null: false

      t.timestamps
    end

    add_index :tasks, [:name, :description], name: 'tasks_search', type: :fulltext
  end
end
