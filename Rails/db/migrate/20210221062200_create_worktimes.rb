class CreateWorktimes < ActiveRecord::Migration[6.1]
  def change
    create_table :worktimes do |t|
      t.string :name, null: false, limit: 45
      t.string :description, null: false, limit: 255
      t.string :problem, limit: 255
      t.string :user,null:false, limit: 255
      t.integer :duration,default: 0
      t.datetime :start_dt, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :end_dt

      t.boolean :archived, null: false, default: false

      t.timestamps
    end
    
    add_index :worktimes, [:name, :description], name: 'worktimes_search', type: :fulltext
  end
end
