class AddUserToTeams < ActiveRecord::Migration[6.1]
  def change
    add_reference :teams, :user, null: false, foreign_key: true
    rename_column :teams, :user_id, :owner_id
  end
end
