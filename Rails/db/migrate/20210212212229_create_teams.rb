class CreateTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :teams do |t|
      t.string :name, null: false, limit: 45
      t.string :description, null: false, limit: 255

      t.string :message, limit: 255
      t.datetime :message_expiration

      t.timestamps
    end

    add_index :teams, [:name, :description], name: 'teams_search', type: :fulltext
  end
end
