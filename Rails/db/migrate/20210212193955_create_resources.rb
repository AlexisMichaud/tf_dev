class CreateResources < ActiveRecord::Migration[6.1]
  def change
    create_table :resources do |t|
      t.string :brand, null: false, limit: 45
      t.string :model, null: false, limit: 45
      t.string :description, null: false, limit: 255

      t.timestamps
    end

    add_index :resources, [:brand, :model, :description], name: 'resources_search', type: :fulltext
  end
end