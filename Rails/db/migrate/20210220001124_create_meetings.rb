class CreateMeetings < ActiveRecord::Migration[6.1]
  def change
    create_table :meetings do |t|
      t.string :name, null: false, limit: 45
      t.string :summary, null: false, limit: 45
      t.string :location, null: false, limit: 45
      t.datetime :start_dt, null: false
      t.datetime :end_dt, null: false
      t.boolean :archived, null: false, default: false

      t.timestamps
    end

    add_index :meetings, [:name, :summary, :location], name: 'meetings_search', type: :fulltext
  end
end
