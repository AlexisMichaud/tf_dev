class AddProjectToMeetings < ActiveRecord::Migration[6.1]
  def change
    add_reference :meetings, :project, null: false, foreign_key: true
  end
end
