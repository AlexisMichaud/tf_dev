class CreateJobOffers < ActiveRecord::Migration[6.1]
  def change
    create_table :job_offers do |t|
      t.string :name, null: false, limit: 45
      t.string :description, null: false, limit: 255
      t.integer :availability, null: false, default: 0
      t.integer :nb_employee_looking_for, null: false, default: 1

      t.timestamps
    end


    add_index :job_offers, [:name, :description], name: 'job_offers_search', type: :fulltext
  end
end
