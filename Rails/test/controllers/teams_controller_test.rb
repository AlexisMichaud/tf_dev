require "test_helper"

class Api::TeamControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = User.first
    sign_in @user
  end

  test "create ok" do
    query = { name: "The A Team Book", description: "best team ever" }
    post "/api/teams", params: { team: query }, as: :json
    assert_response :created

    team = Team.find(3)
    assert_equal query[:name], team.name
    assert_equal query[:description], team.description

    expected = { success: true, data: team }.as_json
    assert_equal expected, @response.parsed_body
  end

  test "cant create with no name" do
    query = { description: "best team ever" }
    post "/api/teams", params: { team: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["errors"]["name"]
    assert_raises ActiveRecord::RecordNotFound do
      Team.find(3)
    end
  end

  test "cant create with no description" do
    query = { name: "The A Team "}
    post "/api/teams", params: { team: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["errors"]["description"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(3)
    end
  end

  test "create description too long" do
    query = { name: "The A Team", description: "z" * 256 }
    post "/api/teams", params: { team: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["errors"]["description"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(3)
    end
  end

  test "update ok" do
    query = { name: "The B team", description: "best team ever" }
    patch "/api/teams/1", params: { team: query }, as: :json
    assert_response :success

    resource = Team.find(1)
    assert_equal query[:name], team.name
    assert_equal query[:description], team.description

    expected = { success: true, data: team }.as_json
    assert_equal expected, @response.parsed_body
  end

  test "update name too long" do
    query = { name: "a" * 100, description: "best team ever" }
    patch "/api/teams/1", params: { team: query }, as: :json
    assert_response :unprocessable_entity

    resource = Team.find(1)
    assert_not_equal query[:name], team.name
    assert_not_equal query[:description], team.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["errors"]["model"]
  end

  test "update description too long" do
    query = {name: "The B team", description: "a" * 256 }
    patch "/api/teams/1", params: { team: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:name], team.name
    assert_not_equal query[:description], team.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["errors"]["description"]
  end

  test "delete ok" do
    expected = { success: true, data: Team.find(1) }.as_json

    delete "/api/teams/1"
    assert_response :success

    assert_equal expected, @response.parsed_body
    assert_raises ActiveRecord::RecordNotFound do
      Team.find(1)
    end
  end

  test "delete not found" do
    delete "/api/team/10"
    assert_response :not_found
    assert_not @response.parsed_body["success"]
  end
end
