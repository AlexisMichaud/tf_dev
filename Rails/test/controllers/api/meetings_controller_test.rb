require "test_helper"

class Api::MeetingsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        @user = User.first
        sign_in @user
        @current_user = User.first
    end

    test "should create meeting" do
        assert_difference -> { Meeting.count } do
            post api_meetings_url,
            params: {
                meeting: {
                name: "meeting.name",
                summary: "meeting.summary",
                location: "meeting.location",
                start_dt: "2021-08-26 01:22:00",
                end_dt: "2021-08-26 03:22:00",
                project_id: 1,
                archived: true },
            }, as: :json
        end
    
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(Meeting.last.as_json, response.parsed_body['data'])
    end

    test "shouldn't create meeting" do
        assert_no_difference -> { Meeting.count } do
        post api_meetings_url,
            params: {
            meeting: {
                name: "meeting.name",
                summary: "meeting.summary",
                location: "meeting.location",
                start_dt: "2021-01-16 01:01:01",                 # Date in past
                end_dt: "2021-01-16 01:01:01",                 # Date in past
                project_id: 1,
                archived: true },
            }, as: :json
        end
    
        assert_response :unprocessable_entity
        assert_equal(false, response.parsed_body['success'])
        assert_not_equal(Meeting.last, response.parsed_body['data'])
    end


    test "should get meeting detail" do
        get api_meeting_url(Meeting.first)

        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(Meeting.first.as_json, response.parsed_body['data'].as_json)
    end

    test "shouldn't get meeting detail" do
        get api_meeting_url(333)

        assert_response :not_found
        assert_equal(false, response.parsed_body['success'])
    end


    test "should update meeting" do
        patch api_meeting_url(Meeting.first), 
        params: {
            meeting: {
            name: "meeting.namee",
            summary: "meeting.summaryy",
            location: "meeting.locationn",
            start_dt: "2021-07-26 01:23:00",
            end_dt: "2021-07-26 03:23:00",
            project_id: 1,
            archived: true },
        }, as: :json
        
        assert_response :success
        assert_equal(Meeting.first.as_json, response.parsed_body['data'].as_json)
        assert_not_equal(response.parsed_body['data']['created_at'], response.parsed_body['data']['updated_at'])
    end

    test "shouldn't update meeting" do
        patch api_meeting_url(Meeting.first),
        params: {
                meeting: {
                name: "pprojectn",
                summary: "mmeetings",
                location: "mmeetingl",
                start_dt: "2021-01-24",                 # Date in past
                end_dt: "2021-01-24",
                archived: "pprojecta",
                project_id: 1 },
        }, as: :json

        assert_response :unprocessable_entity
        assert_not_equal(Meeting.first.as_json, response.parsed_body['data'].as_json)
        assert_equal(false, response.parsed_body['success'])
    end


    test "should delete meeting" do
        assert_difference 'Meeting.count', -1 do
            delete api_meeting_url(Meeting.find(1)), as: :json
        end
        
        assert_raises ActiveRecord::RecordNotFound do
            Meeting.find(1)
        end
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
    end

    test "shouldn't delete meeting" do
        assert_no_difference 'Meeting.count' do
            delete api_meeting_url(Meeting.find(2)), as: :json
        end
        
        assert_response :not_found
        assert_equal(false, response.parsed_body['success'])
    end


    test "should search meetings" do
        get api_meetings_url, params: { 'search': 'Sl*', 'project': 1 }

        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(Project.first.meetings.first.as_json, response.parsed_body['data'].first)
    end

    test "shouldn't search meetings" do
        get api_meetings_url, params: { 'search': 'z', 'project': 2 }
        
        assert_response :not_found
        assert_equal(false, response.parsed_body['success'])
        assert_nil(response.parsed_body['data']['meetings'])
    end


    test "should filter meetings" do
        get api_meetings_url, params: { 'status': 'active', 'project': 1 }
        
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(1, response.parsed_body['data'].count)
    end

    test "shouldn't filter meetings" do
        get api_meetings_url, params: { 'status': 'active', 'project': 2 }
        
        assert_response :not_found
        assert_equal(false, response.parsed_body['success'])
        assert_nil(response.parsed_body['data']['meetings'])
    end
end
