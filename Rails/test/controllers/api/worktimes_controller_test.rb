require "test_helper"

class Api::WorktimesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = User.first
    sign_in @user
  end

  test "Should create worktime" do
      assert_difference -> { Worktime.count } do
      post api_worktimes_url,  
      params: { 
          worktime: { 
              name: "MyLastString",
              description: "MyString",
              archived: false,
              start_dt: "2021-02-21 01:22:00",
              end_dt: "2021-02-21 01:35:00",
              task_id: tasks(:one).id,
              user_id: users(:one).id} 
              },as: :json
      end
      assert_response :success
      assert_equal(response.parsed_body["data"], Worktime.last.as_json)
  end
  

  test "should not create worktime" do
    assert_no_difference -> { Worktime.count } do
        post api_worktimes_url,  
        params: { 
            worktime: { 
                name: "MyLastStringaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                description: "MyString",
                archived: false,
                start_dt: "2021-02-21 01:22:00",
                end_dt: "2021-02-21 01:35:00",
                task_id: tasks(:one).id,
                user_id: users(:one).id} 
                },as: :json
        end
        assert_response :unprocessable_entity
        assert_not_equal(response.parsed_body["data"], Worktime.last.as_json)
  end

  test "should update worktime" do
      patch api_worktime_url(worktimes(:one)),  
      params: { 
          worktime: { 
                name: "MyUpdatedString",
                description: "MyString",
                archived: false,
                start_dt: "2021-02-21 01:22:00",
                end_dt: "2021-02-21 01:35:00",
                task_id: tasks(:one).id,
                user_id: users(:one).id}  
              },as: :json
      assert_response :success
      assert_equal( response.parsed_body["data"],Worktime.find(worktimes(:one).id).as_json )
  end

  test "should not update worktime" do
    patch api_worktime_url(worktimes(:one)),  
      params: { 
          worktime: { 
                name: "MyUpdatedStringFailedStringCauseItsTooLongAndAbove45Characters",
                description: "MyString",
                archived: false,
                start_dt: "2021-02-21 01:22:00",
                end_dt: "2021-02-21 01:35:00",
                task_id: tasks(:one).id,
                user_id: users(:one).id}  
              },as: :json
      assert_response :unprocessable_entity
      assert_not_equal( response.parsed_body["data"],Worktime.find(worktimes(:one).id).as_json )
  end

  test "Should delete worktime" do
      assert_difference -> { Worktime.count }, -1 do
          delete api_worktime_url(worktimes(:one))  
      end
      assert_response :success
  end

  test "Should not delete worktime" do
      assert_no_difference -> { Worktime.count } do
          delete api_worktime_url(5)  
      end
      assert_response :missing
  end

  test "Should list worktimes" do
      get api_worktimes_url,params: { 'id': '1' }
      assert_response :success
      assert_equal(response.parsed_body["data"],Worktime.all.as_json)
  end


  test "Should read worktime" do
      get api_worktime_url(worktimes(:one))
      assert_response :success
      assert_equal(response.parsed_body["data"],Worktime.find(worktimes(:two).id).as_json)
  end

  test "Should not read worktime" do
    get api_worktime_url(3)
    assert_response :not_found
  end
 

  test "should filter worktimes" do
    get api_worktimes_url, params: { 'id': '1','archived': "true" }
    assert_response :success

    assert_equal(response.parsed_body['data'],[worktimes(:two).as_json])
  end

  test "should not filter worktimes" do
    get api_worktimes_url, params: { 'id': '1','archived': "testing" }
    assert_response :success
    assert_equal(response.parsed_body["data"],[Worktime.first.as_json])
  end

  test "should search worktimes" do
    get api_worktimes_url, params: { 'id': '1','search': 'First' }
    assert_response :success
    assert_equal(response.parsed_body['data'],[worktimes(:one).as_json])
  end

  test "should not find any worktimes" do
    get api_worktimes_url, params: { 'id': '1','search': '' }
    assert_response :success
    assert_equal(response.parsed_body['data'],[])
  end

end
