require "test_helper"

class Api::MembershipsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
      @user = User.first
      sign_in @user
    end



    test "Should create membership" do
        assert_difference -> { Membership.count } do
        post api_memberships_url,  
        params: { 
            mambership: { 
                user_id: users(:three).id
                team_id: teams(:one).id} 
                },as: :json
        end
        assert_response :success
        assert_equal(response.parsed_body["data"], Membership.last.as_json)
    end
    
  
    test "should not create membership user already have team" do
      assert_no_difference -> { Team.count } do
          post api_worktimes_url,  
          params: { 
            mambership: { 
                user_id: users(:one).id
                team_id: teams(:one).id} 
                },as: :json
          end
          assert_response :unprocessable_entity
          assert_not_equal(response.parsed_body["data"], Team.last.as_json)
    end
  
  
    test "delete ok" do
        expected = { success: true, data: Membership.find(1) }.as_json
    
        delete "/api/memberships/1"
        assert_response :success
    
        assert_equal expected, @response.parsed_body
        assert_raises ActiveRecord::RecordNotFound do
            Membership.find(1)
        end
      end
    
      test "delete not found" do
        delete "/api/memberships/44"
        assert_response :not_found
        assert_not @response.parsed_body["success"]
      end

end