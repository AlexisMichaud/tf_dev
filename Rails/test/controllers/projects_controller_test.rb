require "test_helper"

class ProjectsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
      @user = User.first
      sign_in @user
      @current_user = User.first
    end
    
    test "should create project" do
        assert_difference -> { Project.count } do
          post api_projects_url,
            params: {
              project: {
                name: "project.name",
                description: "project.description",
                due_date: "2021-06-16",
                archived: true },
            }, as: :json
        end
    
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(Project.last.as_json, response.parsed_body['data'])
    end

    test "shouldn't create project" do
        assert_no_difference -> { Project.count } do
        post api_projects_url,
            params: {
            project: {
                name: "project.name",
                description: "project.description",
                due_date: "2021-01-16",                 # Date in past
                archived: "project.archived" },
            }, as: :json
        end
    
        assert_response 422
        assert_equal(false, response.parsed_body['success'])
        assert_not_equal(Project.last.as_json, response.parsed_body['data'])
    end


    test "should get project detail" do
        get api_project_url(Project.first)
        
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(Project.first.as_json, response.parsed_body['data'])
    end

    test "shouldn't get project detail" do
        get api_project_url(333)

        assert_response :not_found
        assert_equal(false, response.parsed_body['success'])
    end


    test "should update project" do
      patch api_project_url(Project.first), 
        params: {
            project: {
            name: "projectn",
            description: "projectd",
            due_date: "2021-08-24",
            archived: true },
        }, as: :json
        
        assert_response :success
        assert_equal(Project.first.as_json, response.parsed_body['data'])
        assert_not_equal(response.parsed_body['data']['created_at'], response.parsed_body['data']['updated_at'])
    end

    test "shouldn't update project" do
      patch api_project_url(Project.first),
        params: {
            project: {
            name: "pprojectn",
            description: "pprojectd",
            due_date: "2021-01-16",                 # Date in past
            archived: "pprojecta" },
        }, as: :json
        
        assert_response :unprocessable_entity
        assert_not_equal(Project.first, response.parsed_body['data'])
        assert_equal(false, response.parsed_body['success'])
    end


    test "should delete project" do
        assert_difference 'Project.count', -1 do
            delete api_project_url(Project.find(1)), as: :json
        end
        
        assert_raises ActiveRecord::RecordNotFound do
            Project.find(1)
        end
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
    end

    test "shouldn't delete project" do
        assert_no_difference 'Project.count' do
            delete api_project_url(Project.find(2)), as: :json
        end
        
        assert_not_nil(Project.find(2))
        assert_response :not_found
        assert_equal(false, response.parsed_body['success'])
    end


    test "should search projects" do
        get api_projects_url, params: { 'search': 'Doi*' }

        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(Project.find(1).as_json, response.parsed_body['data'].first)
    end

    test "shouldn't search projects" do
        get api_projects_url, params: { 'search': 'z' }
        
        assert_response :success
        assert_nil(response.parsed_body['data'].first)
    end


    test "should filter projects" do
        get api_projects_url, params: { 'status': 'active' }
        
        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        assert_equal(1, response.parsed_body['data'].count)
    end


    test "should get analytics projects" do
        get api_projects_analytics_url, params: { 'status': 'all', 'search': 'null', 'date_range': 'null' }

        assert_response :success
        assert_equal(true, response.parsed_body['success'])
        
        # Assures receives projects & stats
        assert_equal(2, response.parsed_body['data'].count) 

        # Assures receive the good fields for the (first) project
        assert_equal(['id', 'name', 'archived', 'description', 'meetings_count'], response.parsed_body['data'].last.as_json.keys) 
    end
end