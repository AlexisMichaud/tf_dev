require "test_helper"

class ResourcesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = User.find(1)
    sign_in @user
  end

  test "index ok" do
    get "/api/resources"
    assert_response :success

    expected = { success: true, data: Resource.all }.as_json({})
    assert_equal expected, @response.parsed_body
  end

  test "index search ok" do
    get "/api/resources?search=Dell"
    assert_response :success

    expected = { success: true, data: Resource.where(brand: "Dell") }.as_json({})
    assert_equal expected, @response.parsed_body
  end

  test "show ok" do
    get "/api/resources/1"
    assert_response :success

    expected = { success: true, data: Resource.find(1) }.as_json
    assert_equal expected, @response.parsed_body
  end

  test "show not fount" do
    get "/api/resources/99"
    assert_response :not_found
    assert_not @response.parsed_body["success"]
  end

  test "create ok" do
    query = { model: "Surface Book", brand: "Microsoft", description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :created

    resource = Resource.find(4)
    assert_equal query[:model], resource.model
    assert_equal query[:brand], resource.brand
    assert_equal query[:description], resource.description

    expected = { success: true, data: resource }.as_json({})
    assert_equal expected, @response.parsed_body
  end

  test "create no model" do
    query = { brand: "Microsoft", description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["model"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create no brand" do
    query = { model: "Surface Book", description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["brand"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create no description" do
    query = { brand: "Microsoft", model: "Surface Book" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["description"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create whitespace model" do
    query = { model: " ", brand: "Microsoft", description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["model"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create whitespace brand" do
    query = { model: "Surface Book", brand: " ", description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["brand"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create whitespace description" do
    query = { brand: "Microsoft", model: "Surface Book", description: " " }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["description"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create model too long" do
    query = { model: "a" * 46, brand: "Microsoft", description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["model"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create brand too long" do
    query = { model: "Surface Book", brand: "a" * 46, description: "2020" }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["brand"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "create description too long" do
    query = { brand: "Microsoft", model: "Surface Book", description: "a" * 256 }
    post "/api/resources", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["description"]
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(4)
    end
  end

  test "update ok" do
    query = { model: "Surface Book", brand: "Microsoft", description: "2020" }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :success

    resource = Resource.find(1)
    assert_equal query[:model], resource.model
    assert_equal query[:brand], resource.brand
    assert_equal query[:description], resource.description

    expected = { success: true, data: resource }.as_json({})
    assert_equal expected, @response.parsed_body
  end

  test "update not found" do
    query = { model: "Surface Book", brand: "Microsoft", description: "2020" }
    patch "/api/resources/99", params: { resource: query }, as: :json
    assert_response :not_found
    assert_not @response.parsed_body["success"]
  end

  test "update whitespace model" do
    query = { model: " ", brand: "Microsoft", description: "2020" }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:model], resource.model
    assert_not_equal query[:brand], resource.brand
    assert_not_equal query[:description], resource.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["model"]
  end

  test "update whitespace brand" do
    query = { model: "Surface Book", brand: " ", description: "2020" }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:model], resource.model
    assert_not_equal query[:brand], resource.brand
    assert_not_equal query[:description], resource.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["brand"]
  end

  test "update whitespace description" do
    query = { brand: "Microsoft", model: "Surface Book", description: " " }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:model], resource.model
    assert_not_equal query[:brand], resource.brand
    assert_not_equal query[:description], resource.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["description"]
  end

  test "update model too long" do
    query = { model: "a" * 46, brand: "Microsoft", description: "2020" }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:model], resource.model
    assert_not_equal query[:brand], resource.brand
    assert_not_equal query[:description], resource.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["model"]
  end

  test "update brand too long" do
    query = { model: "Surface Book", brand: "a" * 46, description: "2020" }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:model], resource.model
    assert_not_equal query[:brand], resource.brand
    assert_not_equal query[:description], resource.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["brand"]
  end

  test "update description too long" do
    query = { brand: "Microsoft", model: "Surface Book", description: "a" * 256 }
    patch "/api/resources/1", params: { resource: query }, as: :json
    assert_response :unprocessable_entity

    resource = Resource.find(1)
    assert_not_equal query[:model], resource.model
    assert_not_equal query[:brand], resource.brand
    assert_not_equal query[:description], resource.description

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["description"]
  end

  test "delete ok" do
    expected = { success: true, data: Resource.find(1) }.as_json

    delete "/api/resources/1"
    assert_response :success

    assert_equal expected, @response.parsed_body
    assert_raises ActiveRecord::RecordNotFound do
      Resource.find(1)
    end
  end

  test "delete not found" do
    delete "/api/resources/99"
    assert_response :not_found
    assert_not @response.parsed_body["success"]
  end

  test "borrow ok" do
    expected = {
      id: 3,
      resource_id: 3,
      start_date: Date.current,
      end_date: nil,
      state: nil,
      state_id: nil,
      state_name: nil,
      user_id: @user.id,
      user_fullname: @user.fullname
    }.as_json

    post "/api/resources/3/borrow"
    assert_response :created

    assert_equal expected, @response.parsed_body["data"]["borrows"][0]
  end

  test "borrow not found" do
    post "/api/resources/99/borrow"
    assert_response :not_found
    assert_not @response.parsed_body["success"]
  end

  test "borrow already borrowed self" do
    post "/api/resources/1/borrow"
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_equal "This resource is not available", @response.parsed_body["data"]
  end

  test "borrow already borrowed other" do
    post "/api/resources/2/borrow"
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_equal "This resource is not available", @response.parsed_body["data"]
  end

  test "unborrow ok" do
    post "/api/resources/1/unborrow", params: { borrow: { state: 1 } }, as: :json
    assert_response :success

    borrow = @response.parsed_body["data"]["borrows"][0]
    assert_equal 1, borrow["state"]
    assert_equal Date.current.to_s(:db), borrow["end_date"]
  end

  test "unborrow invalid state" do
    post "/api/resources/1/unborrow", params: { borrow: { state: 3 } }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_not_empty @response.parsed_body["data"]["state"]
  end

  test "unborrow not borrowed" do
    post "/api/resources/3/unborrow", params: { borrow: { state: 1 } }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_equal "You are not the current owner of this resource", @response.parsed_body["data"]
  end

  test "unborrow borrowed other" do
    post "/api/resources/2/unborrow", params: { borrow: { state: 1 } }, as: :json
    assert_response :unprocessable_entity

    assert_not @response.parsed_body["success"]
    assert_equal "You are not the current owner of this resource", @response.parsed_body["data"]
  end
end
