require "test_helper"

class TasksControllerTest < ActionDispatch::IntegrationTest

    include Devise::Test::IntegrationHelpers
    setup do
      @user = User.first
      sign_in @user
    end

    test "Should create task" do
        assert_difference -> { Task.count } do
        post api_tasks_url,  
        params: { 
            task: { 
                name: "MyLastString",
                description: "MyString",
                state: 0,
                estimated_time: 130,
                actual_time: 30,
                due_date: "2021-05-25",
                project_id: projects(:one).id } 
                },as: :json
        end
        assert_response :success
        assert_equal(response.parsed_body["data"], Task.last.as_json)
    end
    

    test "should not create task" do
        assert_no_difference -> { Task.count } do
        post api_tasks_url,  
        params: { 
            task: { 
                name: "",
                description: "MyString",
                state: 0,
                estimated_time: 130,
                actual_time: 30,
                due_date: "2021-05-25",
                project_id: projects(:one).id } 
                },as: :json
        end
        assert_response :unprocessable_entity
        assert_not_equal(response.parsed_body["data"],Task.last.as_json)
    end

    test "should update task" do
        patch api_task_url(tasks(:one)),  
        params: { 
            task: { 
                name: "MyUpdatedString",
                description: "MyString",
                state: 0,
                estimated_time: 130,
                actual_time: 30,
                due_date: "2021-05-23",
                project_id: projects(:one).id } 
                },as: :json
        assert_response :success
        assert_equal( response.parsed_body["data"],Task.find(tasks(:one).id).as_json )
    end

    test "should not update task" do
        patch api_task_url(tasks(:one)),  
        params: { 
            task: { 
                name: "MyUpdatedStringFailedStringCauseItsTooLongAndAbove45Characters",
                description: "MyString",
                state: 0,
                estimated_time: 130,
                actual_time: 30,
                due_date: "2021-05-23",
                project_id: projects(:one).id } 
                },as: :json
        assert_response :unprocessable_entity
        assert_not_equal( response.parsed_body["data"], Task.find(tasks(:one).id).as_json )
    end

    test "Should delete task" do
        assert_difference -> { Task.count }, -1 do
            delete api_task_url(tasks(:one))  
        end
        assert_response :success
    end

    test "Should not delete task" do
        assert_no_difference -> { Task.count } do
            delete api_task_url(5)  
        end
        assert_response :missing
    end

    test "Should list tasks" do
        get api_tasks_url  
        assert_response :success
        assert_equal(response.parsed_body["data"],[Task.last.as_json])
    end


    test "Should read tasks" do
        get api_task_url(tasks(:one))
        assert_response :success
        assert_equal(response.parsed_body["data"],Task.find(tasks(:one).id).as_json)
    end
        
end