Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  
  root to: "angular#index"

  devise_scope :user do
    get "isloggedin", to: "users/sessions#isLoggedIn"
  end

  namespace :api, constraints: { format: 'json' }  do
    get '/resources/analytics', to: 'resources#analytics'
    resources :resources
    get '/tasks/analytics', to: 'tasks#analytics'
    resources :tasks
    resources :worktimes
    get '/projects/analytics', to: 'projects#analytics' # Had to be there, otherwise won't work. Thanks to leave it here :)
    resources :projects
    resources :meetings
    get '/teams/analytics', to: 'teams#analytics'
    resources :teams
    resources :memberships
    resources :job_offers
 
    post '/project/:id/archive', to: 'projects#archive'
    post '/meeting/:id/archive', to: 'meetings#archive'

    post '/task/:id/archive', to: 'tasks#archive'
    post '/worktime/:id/archive', to: 'worktimes#archive'
    get '/project/:id/tasks', to: 'tasks#project_tasks'
    get '/task/:id/worktimes', to: 'worktimes#index'

    get '/teams/:id/member', to: 'teams#findMember'

    post '/resources/:id/borrow', to: 'resources#borrow'
    post '/resources/:id/unborrow', to: 'resources#unborrow'

    get '/avatar', to: 'profile#show_avatar'
    post '/avatar', to: 'profile#update_avatar'

    get '/home', to: 'home#index'
  end

  match '*url', to: "angular#index", via: :get # le parametre url contiendra tout ce qui suit l'étoile dans l'url
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
