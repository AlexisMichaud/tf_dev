class CustomFailure < Devise::FailureApp
    def respond
        # Verifier si correspond à notre standard de sortie JSON
        self.status = 401 
        self.content_type = 'json'
        self.response_body = {"errors" => ["Invalid login credentials"]}.to_json
    end
end